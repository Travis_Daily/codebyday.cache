# Cache By Day #

Repo for the *Cache By Day* NuGet package: [nuget.org/packages/CodeByDay.Cache](https://www.nuget.org/packages/CodeByDay.Cache)

Adds an Object which will act as a cache. Provide it a key and a function to populate the value.

# Release Notes #

##### 1.3.0 [2020 Sep 21] ([changes](https://bitbucket.org/Travis_Daily/codebyday.cache/branches/compare/1.3.0%0D1.2.0?w=1#diff))

* Added explicit support for .NET 4.6.1 and .NET Standard 2.0 as urged by the [guidance](https://docs.microsoft.com/en-us/dotnet/standard/library-guidance/cross-platform-targeting).

* Removed support for .NET 3.5 and 4.0. Stay on 1.2.0 if you need to support them.

* Added GetAsync().

* Added ITimedCache overloads for ValueTuples.

##### 1.2.0 [2019 Apr 23] ([changes](https://bitbucket.org/Travis_Daily/codebyday.cache/branches/compare/1.2.0%0D1.1.1?w=1#diff))

* Added WeakDictionaryStorage which can be used to make any kind of cache store its items as weak references.

* Obsoleted WeakCache. Use 'new Cache<TKey, TValue>(new WeakDictionaryStorage<TKey, BaseCachedItem<TValue>>())' instead.

##### 1.1.1 [2018 Oct 21] ([changes](https://bitbucket.org/Travis_Daily/codebyday.cache/branches/compare/1.1.1%0D1.1.0?w=1#diff))

* TimedCache and TimedItemedCache will now always mark expired items as 'invalid'. Previously, if IsValid was true during serialization, and the Item expired before being deserialized, IsValid would stay 'true'. Now it will properly become 'false'.

##### 1.1.0 [2018 Jan 19] ([changes](https://bitbucket.org/Travis_Daily/codebyday.cache/branches/compare/1.1.0%0D1.0.1?w=1#diff))

* Enabled serialization on the native ICachedItem objects.

##### 1.0.1 [2018 Jan 14] ([changes](https://bitbucket.org/Travis_Daily/codebyday.cache/branches/compare/1.0.1%0D1.0.0?w=1#diff))

* Fixed an issue where TimedCaches wouldn't return the value they stored when you specified the time to keep.

# Usage Guide #

## Cache Kinds ##

Kinds of Caches available natively:

1. Cache: Stores all items.

2. TimedCache: Stores all items for a given TimeSpan. A TimeSpan can also be specified when Getting/Setting a cached item.

3. ItemedCache: Stores items up to the given limit.

4. TimedItemedCache: Stores items up to the given limit for a given TimeSpan.

Additionally, you can pass a WeakDictionaryStorage object into the constructor of any of these caches. This will allow storage weak references which will be removed as needed by the Garbage Collector.

## Creating a Cache ##

A cache is created like any other object:

```
#!c#
ICache<string, int> cache = new Cache<string, int>();

```

## Using a Cache ##

Putting items into the cache and getting items out of the cache is handled by the ICache<K, V>.Get() function. This function takes the key as the first parameter, and a function which returns a value if the value cannot be found.

```
#!c#
var str = "Test String";
var length = cache.Get(str, () => str.Length);

```

In this example, since there is no existing value for our key "Test String", the function passed in will be executed, and the result of that function will be both stored in the cache and returned from the ICache<K, V>.Get() call. Any subsequent calls with a key value of "Test String" will not evaluate the function, and will instead return the cached value.

```
#!c#
var str = "Test String";
cache.Get(str, () => str.Length);
var length = cache.Get(str, () => -1);
// length == 11

```

This function is the also way to prime the cache. You can call ICache<K, V>.Get() and not read the result. This will store the value.

Also available is ICache<K, V>.TryGet(). This will return 'true' when a cached value is found and will populate the out param. If 'false' is returned, then the value of the out param is undefined.

```
#!c#
if (cache.TryGet(str, out int intOut))
{
  // Use value.
}
else
{
  // Error.
}

```

## Clearing a Cache ##

ICache.Clear() will remove all keys from the cache. You can use this to force all your cache code to reevaluate the results. If you know only one item needs to be removed, you can call ICache<K>.Remove(Key). This function can take an IEnumerable of Keys or you can pass keys one by one as params:

```
#!c#
cache.Remove(key0);
cache.Remove(key1, key2, key3);
cache.Remove(new List<Key>{ key4, key5, key6 });

```

ICache.Clear() and ICache.Remove() block the thread that calls it.

The current implementation of the Cache in Cache By Day will run a task to clean the Cache during a call to Get() when the Cache knows that there are invalid values.

## ExecuteCached ##

Func<>.ExecuteCached() is a set of extensions methods to Func<> that will execute the Func<> and store the result in a cache. These cached values are stored per Func<> object and params passed in, so calling ExecuteCached() with two functions which happen to do the same thing will still cause each of the functions to be executed.

```
#!c#
Func<string> f1 = () => {
  System.Console.WriteLine("f1");
  return "f1";
};

Func<string> f2 = () => {
  System.Console.WriteLine("f1");
  return "f1";
};

f1.ExecuteCached();
f1.ExecuteCached();
System.Console.WriteLine("---")
f2.ExecuteCached();
f2.ExecuteCached();
```

This will print:

```
#!c#
f1
---
f1
```

There are also overloads that take objects to pass into the function.

Note that using these extensions means your Func<> objects and their results will be kept in memory: this is a memory leak! However, you can manually clear these object by calling FuncExtensions.Clear().

## IStorage ##

Cached items are stored in an IStorage. By default a dictionary is used, but a custom object can be used instead. Using a custom object allows the control of the storage of the items, such as saving them into a database.

# Supported .Net Versions #

* .Net Framework 4.5+
* [.Net Standard 1.1](https://docs.microsoft.com/en-us/dotnet/standard/net-standard#net-platforms-support)+

Naturally, anything newer than .Net Framework 4.5 or .Net Standard 1.1 is also supported.

# Repo #

## Branches ##

The branches in this repo relate directly to the release they are building up to. Once a release has been made, the branch is no longer commited to. This effectively means there is no "master" branch: only releases.

## Versioning ##

This project uses [Semantic Versioning 2.0.0](https://semver.org)

If you're making a NuGet package, I recommend editing your .csproj to force this Semantic Versioning. This example requires a version equal to or above 1.3.0 and disallows 2.0.0 or above.

```
#!c#
<PackageReference Include="CodeByDay.Cache" Version="[1.3.0,2.0.0)" />
```

# Have an Issue? #

If you find any issues with this NuGet, please use the [Contact owners](https://www.nuget.org/packages/CodeByDay.Cache/1.3.0/ContactOwners) link on the NuGet page.