﻿using CodeByDay.Cache.CachedItem;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CodeByDay.Cache.Test
{
    public abstract class SerializationTest
    {
        private static JsonSerializerSettings SerializerSettings = new JsonSerializerSettings
        {
            DefaultValueHandling = DefaultValueHandling.Ignore
        };

        public static IEnumerable<TestCaseData> SerializationTests
        {
            get
            {
                IEnumerable<TestCaseData> Yielder<T>(T value)
                {
                    yield return new TestCaseData(new BaseCachedItem<T> { Value = value }, value);

                    yield return new TestCaseData(new CreatedCachedItem<T> { Value = value }, value);

                    yield return new TestCaseData(new TimedCachedItem<T> { Value = value, TimeToKeep = TimeSpan.FromSeconds(30) }, value);

                    yield return new TestCaseData(new UsefulCachedItem<T> { Value = value, AccessCount = 10, LastUsefulness = .5 }, value);

                    yield return new TestCaseData(new FullyCachedItem<T> { Value = value, TimeToKeep = TimeSpan.FromSeconds(30), AccessCount = 10, LastUsefulness = .5 }, value);
                }

                return Yielder(100)
#if HAM
                    .Concat(Yielder("val")).Concat(Yielder(1.1))
#endif
                    ;
            }
        }

        [TestCaseSource(nameof(SerializationTests))]
        public void ICachedItemSerialization<T, Q>(T cachedItem, Q val) where T : ICachedItem<Q>
        {
            var json = JsonConvert.SerializeObject(cachedItem, SerializerSettings);
            Assert.AreEqual(json, JsonConvert.SerializeObject(JsonConvert.DeserializeObject<T>(json), SerializerSettings));
            Assert.AreEqual(val, JsonConvert.DeserializeObject<T>(json).Value);
        }

        private string Formatter(string format, params string[] args)
        {
            var vals = String.Format(format, args);
            while (vals.Contains(",,"))
            {
                vals = vals.Replace(",,", ",");
            }
            vals = vals.StartsWith(",") ? vals.Substring(1) : vals;
            vals = vals.EndsWith(",") ? vals.Substring(0, vals.Length - 1) : vals;
            return $"{{{vals}}}";
        }

        public static IEnumerable<TestCaseData> DefaultSerializationTests
        {
            get
            {
                IEnumerable<TestCaseData> Yielder<T>(T value)
                {
                    IEnumerable<TestCaseData> Wielder<Q>(string val, String prefix, Func<T, Q> builder) where Q : ICachedItem<T>
                    {
                        var invalid = "\"IsValid\":false";

                        yield return new TestCaseData($"{prefix},{null},{null}", builder(default(T)));

                        var a = builder(default(T));
                        a.IsValid = false;
                        yield return new TestCaseData($"{prefix},{null},{invalid}", a);

                        var b = builder(value);
                        yield return new TestCaseData($"{prefix},{val},{null}", b);

                        var c = builder(value);
                        c.IsValid = false;
                        yield return new TestCaseData($"{prefix},{val},{invalid}", c);
                    }

                    var createdTime = DateTime.Today;
                    var timeToKeep = TimeSpan.FromDays(1);

                    var createdTimeWord = $"\"CreatedTime\":\"{createdTime.ToString("yyyy-MM-ddTHH:mm:ss.FFFFFFF")}-04:00\"";
                    var timeToKeepWord = $"\"TimeToKeep\":\"{timeToKeep}\"";

                    var accessCount = 10;
                    var accessCountWord = $"\"AccessCount\":{accessCount}";

                    var lastUsefullness = .5d;
                    var lastUsefullnessWord = $"\"LastUsefulness\":{lastUsefullness}";

                    var q = typeof(T).IsPrimitive ? "" : "\"";
                    var valueWord = $"\"Value\":{q}{value}{q}";
                    var baseValueWord = $"\"BaseValue\":{q}{value}{q}";

                    return Wielder(valueWord, "", v => new BaseCachedItem<T> { Value = v })

                        .Concat(Wielder(valueWord, createdTimeWord, v => new CreatedCachedItem<T> { Value = v, CreatedTime = createdTime }))

                        .Concat(Wielder(valueWord, createdTimeWord, v => new TimedCachedItem<T> { Value = v, CreatedTime = createdTime }))
                        .Concat(Wielder(valueWord, timeToKeepWord + "," + createdTimeWord, v => new TimedCachedItem<T> { Value = v, CreatedTime = createdTime, TimeToKeep = timeToKeep }))

                        .Concat(Wielder(baseValueWord, createdTimeWord, v => new UsefulCachedItem<T> { Value = v, CreatedTime = createdTime }))
                        .Concat(Wielder(baseValueWord, accessCountWord + "," + createdTimeWord, v => new UsefulCachedItem<T> { Value = v, CreatedTime = createdTime, AccessCount = accessCount }))
                        .Concat(Wielder(baseValueWord, lastUsefullnessWord + "," + createdTimeWord, v => new UsefulCachedItem<T> { Value = v, CreatedTime = createdTime, LastUsefulness = lastUsefullness }))
                        .Concat(Wielder(baseValueWord, accessCountWord + "," + lastUsefullnessWord + "," + createdTimeWord, v => new UsefulCachedItem<T> { Value = v, CreatedTime = createdTime, AccessCount = accessCount, LastUsefulness = lastUsefullness }))

                        .Concat(Wielder(baseValueWord, createdTimeWord, v => new FullyCachedItem<T> { Value = v, CreatedTime = createdTime }));
                }

                return Yielder(100)
#if HAM
                    .Concat(Yielder("val")).Concat(Yielder(1.1))
#endif
                    ;
            }
        }

        [TestCaseSource(nameof(DefaultSerializationTests))]
        public void DefaultSerializationTest(string roughExpected, object cachedItem)
        {
            var serial = JsonConvert.SerializeObject(cachedItem, SerializerSettings);

            var missings = roughExpected.Split(',').Where(x => !serial.Contains(x));
            if (missings.Any())
            {
                Assert.IsEmpty($"\n{String.Join("\n", missings.ToArray())}\n\n{roughExpected}\n!=\n{serial}");
            }

            var invertedMissings = serial.Split(',').Select(x => x.Replace("{", "").Replace("}", "")).Where(x => !roughExpected.Contains(x));
            if (invertedMissings.Any())
            {
                Assert.IsEmpty($"\n{String.Join("\n", invertedMissings.ToArray())}\n\n{roughExpected}\n!=\n{serial}");
            }
        }
    }
}
