﻿using CodeByDay.Cache.CachedItem;
using CodeByDay.Cache.Test.Infrastructure;
using CodeByDay.Cache.Usefulness;
using NUnit.Framework;
using System;
using System.Threading;

namespace CodeByDay.Cache.Test
{
    public abstract class IItemedCacheTest
    {
        [Vorpal(CacheKinds.Itemed, TypeKinds.All)]
        public void ItemedKeepAndRemoval<TKey, TVal>(IItemedCache<TKey, TVal> cache)
        {
            for (int i = 0; i < (TestCaseProvider.ITERATIONS - TestCaseProvider.ITEMS_TO_KEEP); i++)
            {
                cache.Get(i.To<TKey>(), () => i.To<TVal>());
            }

            Thread.Sleep(1);

            for (int i = (TestCaseProvider.ITERATIONS - TestCaseProvider.ITEMS_TO_KEEP); i < TestCaseProvider.ITERATIONS; i++)
            {
                cache.Get(i.To<TKey>(), () => i.To<TVal>());
            }

            cache.Clean();

            // Since ITERATIONS > LIMIT, the values starting at (ITERATIONS - LIMIT) and moving up must still exits.
            // Assert that trying to get their value DOES NOT run the function.
            for (int i = (TestCaseProvider.ITERATIONS - TestCaseProvider.ITEMS_TO_KEEP); i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual(i.To<TVal>(), cache.Get(i.To<TKey>(), () => (-1).To<TVal>()));
            }

            // Since ITERATIONS > LIMIT, the first (ITERATIONS - LIMIT) values MUST have been removed.
            // Assert that trying to get their value runs the function.
            for (int i = 0; i < (TestCaseProvider.ITERATIONS - TestCaseProvider.ITEMS_TO_KEEP); i++)
            {
                Assert.AreEqual((-1).To<TVal>(), cache.Get(i.To<TKey>(), () => (-1).To<TVal>()));
            }
        }

        [Vorpal(CacheKinds.Itemed, TypeKinds.All)]
        public void SingleItemDelete<TKey, TVal>(IItemedCache<TKey, TVal> cache)
        {
            cache.ItemsToKeep = 1;

            cache.Get(1.To<TKey>(), () => 1.To<TVal>());

            Thread.Sleep(10);

            cache.Get(2.To<TKey>(), () => 2.To<TVal>());

            cache.Clean();

            Assert.AreEqual((-1).To<TVal>(), cache.Get(1.To<TKey>(), () => (-1).To<TVal>()));
        }

        [Vorpal(CacheKinds.Itemed, TypeKinds.All)]
        public void SingleItemKeep<TKey, TVal>(IItemedCache<TKey, TVal> cache)
        {
            cache.ItemsToKeep = 1;

            cache.Get(1.To<TKey>(), () => 1.To<TVal>());

            Thread.Sleep(10);

            cache.Get(2.To<TKey>(), () => 2.To<TVal>());

            cache.Clean();

            Assert.AreEqual(2.To<TVal>(), cache.Get(2.To<TKey>(), () => (-1).To<TVal>()));
        }

        [Vorpal(CacheKinds.Itemed, TypeKinds.All)]
        public void DoubleItemDelete<TKey, TVal>(IItemedCache<TKey, TVal> cache)
        {
            cache.ItemsToKeep = 2;

            cache.Get(1.To<TKey>(), () => 1.To<TVal>());
            cache.Get(2.To<TKey>(), () => 2.To<TVal>());

            Thread.Sleep(10);

            cache.Get(3.To<TKey>(), () => 3.To<TVal>());
            cache.Get(4.To<TKey>(), () => 4.To<TVal>());

            cache.Clean();

            Assert.AreEqual((-1).To<TVal>(), cache.Get(1.To<TKey>(), () => (-1).To<TVal>()));
            Assert.AreEqual((-1).To<TVal>(), cache.Get(2.To<TKey>(), () => (-1).To<TVal>()));
        }

        [Vorpal(CacheKinds.Itemed, TypeKinds.All)]
        public void DoubleItemKeep<TKey, TVal>(IItemedCache<TKey, TVal> cache)
        {
            cache.ItemsToKeep = 2;

            cache.Get(1.To<TKey>(), () => 1.To<TVal>());
            cache.Get(2.To<TKey>(), () => 2.To<TVal>());

            Thread.Sleep(10);

            cache.Get(3.To<TKey>(), () => 3.To<TVal>());
            cache.Get(4.To<TKey>(), () => 4.To<TVal>());

            cache.Clean();

            Assert.AreEqual(3.To<TVal>(), cache.Get(3.To<TKey>(), () => (-1).To<TVal>()));
            Assert.AreEqual(4.To<TVal>(), cache.Get(4.To<TKey>(), () => (-1).To<TVal>()));
        }

        private class InvertedLinearUsefulnessProvider : UsefulnessProvider
        {
            public override double GetUsefulness(IUsefulCachedItem val, DateTime referenceTime)
            {
                var msAlive = Math.Max((referenceTime - val.CreatedTime).TotalMilliseconds, 0.0);
                return -Math.Max(val.AccessCount, 1) / msAlive;
            }
        }

        [Vorpal(CacheKinds.Itemed, TypeKinds.All)]
        public void InvertedDoubleItemDelete<TKey, TVal>(IItemedCache<TKey, TVal> cache)
        {
            cache.UsefulnessProvider = new InvertedLinearUsefulnessProvider();
            cache.ItemsToKeep = 2;

            cache.Get(1.To<TKey>(), () => 1.To<TVal>());
            cache.Get(2.To<TKey>(), () => 2.To<TVal>());

            Thread.Sleep(10);

            cache.Get(3.To<TKey>(), () => 3.To<TVal>());
            cache.Get(4.To<TKey>(), () => 4.To<TVal>());

            cache.Clean();

            Assert.AreEqual((-1).To<TVal>(), cache.Get(3.To<TKey>(), () => (-1).To<TVal>()));
            Assert.AreEqual((-1).To<TVal>(), cache.Get(4.To<TKey>(), () => (-1).To<TVal>()));
        }

        [Vorpal(CacheKinds.Itemed, TypeKinds.All)]
        public void InvertedDoubleItemKeep<TKey, TVal>(IItemedCache<TKey, TVal> cache)
        {
            cache.UsefulnessProvider = new InvertedLinearUsefulnessProvider();
            cache.ItemsToKeep = 2;

            cache.Get(1.To<TKey>(), () => 1.To<TVal>());
            cache.Get(2.To<TKey>(), () => 2.To<TVal>());

            Thread.Sleep(10);

            cache.Get(3.To<TKey>(), () => 3.To<TVal>());
            cache.Get(4.To<TKey>(), () => 4.To<TVal>());

            cache.Clean();

            Assert.AreEqual(1.To<TVal>(), cache.Get(1.To<TKey>(), () => (-1).To<TVal>()));
            Assert.AreEqual(2.To<TVal>(), cache.Get(2.To<TKey>(), () => (-1).To<TVal>()));
        }

        [Vorpal(CacheKinds.Itemed, TypeKinds.All)]
        public void ItemedIncrease<TKey, TVal>(IItemedCache<TKey, TVal> cache)
        {
            cache.ItemsToKeep = TestCaseProvider.ITERATIONS;

            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                cache.Get(i.To<TKey>(), () => i.To<TVal>());
            }

            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual(i.To<TVal>(), cache.Get(i.To<TKey>(), () => (-1).To<TVal>()));
            }
        }

        [Vorpal(CacheKinds.Itemed, TypeKinds.All)]
        public void ItemedDecrease<TKey, TVal>(IItemedCache<TKey, TVal> cache)
        {
            cache.ItemsToKeep = TestCaseProvider.ITERATIONS;

            for (int i = 0; i < (TestCaseProvider.ITERATIONS - TestCaseProvider.ITEMS_TO_KEEP); i++)
            {
                cache.Get(i.To<TKey>(), () => i.To<TVal>());
            }

            Thread.Sleep(1);

            for (int i = (TestCaseProvider.ITERATIONS - TestCaseProvider.ITEMS_TO_KEEP); i < TestCaseProvider.ITERATIONS; i++)
            {
                cache.Get(i.To<TKey>(), () => i.To<TVal>());
            }

            cache.ItemsToKeep = TestCaseProvider.ITEMS_TO_KEEP;

            cache.Clean();

            var validCount = 0;

            for (int i = TestCaseProvider.ITERATIONS - 1; i >= 0; i--)
            {
                if (i.To<TVal>().Equals(cache.Get(i.To<TKey>(), () => (-1).To<TVal>())))
                {
                    validCount++;
                }
            }

            Assert.AreEqual(TestCaseProvider.ITEMS_TO_KEEP, validCount);
        }

        [Vorpal(CacheKinds.Itemed, TypeKinds.All)]
        public void AutoCleanN1<TKey, TVal>(IItemedCache<TKey, TVal> cache)
        {
            cache.ItemsToKeep = -1;

            Assert.AreEqual(0.To<TVal>(), cache.Get(0.To<TKey>(), () => 0.To<TVal>()));

            while (cache is ITestInterfaces u && u.CleanIsRunning())
            {
                Thread.Sleep(1);
            }

            Assert.AreEqual((-1).To<TVal>(), cache.Get(0.To<TKey>(), () => (-1).To<TVal>()));
        }

        [Vorpal(CacheKinds.Itemed, TypeKinds.All)]
        public void AutoClean0<TKey, TVal>(IItemedCache<TKey, TVal> cache)
        {
            cache.ItemsToKeep = 0;

            Assert.AreEqual(0.To<TVal>(), cache.Get(0.To<TKey>(), () => 0.To<TVal>()));

            while (cache is ITestInterfaces u && u.CleanIsRunning())
            {
                Thread.Sleep(1);
            }

            Assert.AreEqual((-1).To<TVal>(), cache.Get(0.To<TKey>(), () => (-1).To<TVal>()));
        }

        [Vorpal(CacheKinds.Itemed, TypeKinds.All)]
        public void AutoClean1<TKey, TVal>(IItemedCache<TKey, TVal> cache)
        {
            cache.ItemsToKeep = 1;

            Assert.AreEqual(0.To<TVal>(), cache.Get(0.To<TKey>(), () => 0.To<TVal>()));

            Thread.Sleep(50);

            Assert.AreEqual(1.To<TVal>(), cache.Get(1.To<TKey>(), () => 1.To<TVal>()));

            while (cache is ITestInterfaces u && u.CleanIsRunning())
            {
                Thread.Sleep(1);
            }

            Assert.AreEqual((-1).To<TVal>(), cache.Get(0.To<TKey>(), () => (-1).To<TVal>()));
        }
    }
}
