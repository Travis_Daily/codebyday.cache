﻿using CodeByDay.Cache.CachedItem;
using CodeByDay.Cache.Storage;
using System;
using System.Collections.Generic;

namespace CodeByDay.Cache.Test.Infrastructure
{
    public static class TestCaseProvider
    {
        public const int ITERATIONS = 10 * 60;

        public const int SPEED_START_ITERATIONS = 100;
        public const int SPEED_END_ITERATIONS = 10_000;
        public const int ALL_INVALIDATE_SPEED_ITERATIONS = 100;

        public const int TIME_TO_KEEP = ITERATIONS / 20;
        public const int BISECTION_POINT = ITERATIONS / 2;

        public const int ITEMS_TO_KEEP = ITERATIONS * 2 / 3;

        public static IEnumerable<ICacheTestCase> GetCachesByType(CacheKinds caches, Type key, Type val)
        {
            return typeof(TestCaseProvider).GetMethod("GetCachesInternal").MakeGenericMethod(key, val).Invoke(null, new Object[] { caches }) as IEnumerable<ICacheTestCase>;
        }

        public static IEnumerable<ICacheTestCase> GetCachesByTypeKinds(CacheKinds caches, TypeKinds types)
        {
            foreach (var typePair in GetTypes(types))
            {
                foreach (var x in typeof(TestCaseProvider).GetMethod("GetCachesInternal").MakeGenericMethod(typePair.Key, typePair.Value).Invoke(null, new Object[] { caches }) as IEnumerable<ICacheTestCase>)
                {
                    yield return x;
                }
            }
        }

        public static IEnumerable<ICacheTestCase> GetCachesInternal<TKey, TVal>(CacheKinds caches)
        {
            switch (caches)
            {
                case CacheKinds.All:
                case CacheKinds.Strong:
                    yield return new ICacheTestCase(new Cache<TKey, TVal>());
                    yield return new ICacheTestCase(new TimedCache<TKey, TVal>());
                    yield return new ICacheTestCase(new ItemedCache<TKey, TVal>() { ItemsToKeep = int.MaxValue });
                    yield return new ICacheTestCase(new TimedItemedCache<TKey, TVal>() { TimeToKeep = TimeSpan.MaxValue, ItemsToKeep = int.MaxValue });
                    if (caches == CacheKinds.All)
                    {
                        goto case CacheKinds.Weak;
                    }
                    else
                    {
                        break;
                    }

                case CacheKinds.Timed:
                    yield return new ICacheTestCase(new TimedCache<TKey, TVal>() { TimeToKeep = TimeSpan.FromMilliseconds(TIME_TO_KEEP) });
                    yield return new ICacheTestCase(new TimedItemedCache<TKey, TVal>() { TimeToKeep = TimeSpan.FromMilliseconds(TIME_TO_KEEP), ItemsToKeep = int.MaxValue });
                    break;

                case CacheKinds.Itemed:
                    yield return new ICacheTestCase(new ItemedCache<TKey, TVal>() { ItemsToKeep = ITEMS_TO_KEEP });
                    yield return new ICacheTestCase(new TimedItemedCache<TKey, TVal>() { TimeToKeep = TimeSpan.MaxValue, ItemsToKeep = ITEMS_TO_KEEP });
                    break;

                case CacheKinds.Weak:
#pragma warning disable CS0618 // Type or member is obsolete
                    yield return new ICacheTestCase(new WeakCache<TKey, TVal>());
#pragma warning restore CS0618 // Type or member is obsolete

                    yield return new ICacheTestCase(new Cache<TKey, TVal>(new WeakDictionaryStorage<TKey, BaseCachedItem<TVal>>()));
                    yield return new ICacheTestCase(new TimedCache<TKey, TVal>(new WeakDictionaryStorage<TKey, TimedCachedItem<TVal>>()) { TimeToKeep = TimeSpan.MaxValue });
                    yield return new ICacheTestCase(new ItemedCache<TKey, TVal>(new WeakDictionaryStorage<TKey, UsefulCachedItem<TVal>>()) { ItemsToKeep = int.MaxValue });
                    yield return new ICacheTestCase(new TimedItemedCache<TKey, TVal>(new WeakDictionaryStorage<TKey, FullyCachedItem<TVal>>()) { TimeToKeep = TimeSpan.MaxValue, ItemsToKeep = int.MaxValue });
                    break;

                case CacheKinds.AlwaysEqual:
                    yield return new ICacheTestCase(new Cache<TKey, TVal>(new DictionaryStorage<TKey, BaseCachedItem<TVal>>(new AlwaysEqual<TKey>())));
                    yield return new ICacheTestCase(new TimedCache<TKey, TVal>(new DictionaryStorage<TKey, TimedCachedItem<TVal>>(new AlwaysEqual<TKey>())) { TimeToKeep = TimeSpan.MaxValue });
                    yield return new ICacheTestCase(new ItemedCache<TKey, TVal>(new DictionaryStorage<TKey, UsefulCachedItem<TVal>>(new AlwaysEqual<TKey>())) { ItemsToKeep = int.MaxValue });
                    yield return new ICacheTestCase(new TimedItemedCache<TKey, TVal>(new DictionaryStorage<TKey, FullyCachedItem<TVal>>(new AlwaysEqual<TKey>())) { TimeToKeep = TimeSpan.MaxValue, ItemsToKeep = int.MaxValue });

#pragma warning disable CS0618 // Type or member is obsolete
                    yield return new ICacheTestCase(new WeakCache<TKey, TVal>(new AlwaysEqual<TKey>()));
#pragma warning restore CS0618 // Type or member is obsolete

                    yield return new ICacheTestCase(new Cache<TKey, TVal>(new WeakDictionaryStorage<TKey, BaseCachedItem<TVal>>(new AlwaysEqual<TKey>())));
                    yield return new ICacheTestCase(new TimedCache<TKey, TVal>(new WeakDictionaryStorage<TKey, TimedCachedItem<TVal>>(new AlwaysEqual<TKey>())) { TimeToKeep = TimeSpan.MaxValue });
                    yield return new ICacheTestCase(new ItemedCache<TKey, TVal>(new WeakDictionaryStorage<TKey, UsefulCachedItem<TVal>>(new AlwaysEqual<TKey>())) { ItemsToKeep = int.MaxValue });
                    yield return new ICacheTestCase(new TimedItemedCache<TKey, TVal>(new WeakDictionaryStorage<TKey, FullyCachedItem<TVal>>(new AlwaysEqual<TKey>())) { TimeToKeep = TimeSpan.MaxValue, ItemsToKeep = int.MaxValue });
                    break;

                case CacheKinds.EvenOddEqual:
                    yield return new ICacheTestCase(new Cache<int, int>(new DictionaryStorage<int, BaseCachedItem<int>>(new EvenOddEqual())));
                    yield return new ICacheTestCase(new TimedCache<int, int>(new DictionaryStorage<int, TimedCachedItem<int>>(new EvenOddEqual())) { TimeToKeep = TimeSpan.MaxValue });
                    yield return new ICacheTestCase(new ItemedCache<int, int>(new DictionaryStorage<int, UsefulCachedItem<int>>(new EvenOddEqual())) { ItemsToKeep = int.MaxValue });
                    yield return new ICacheTestCase(new TimedItemedCache<int, int>(new DictionaryStorage<int, FullyCachedItem<int>>(new EvenOddEqual())) { TimeToKeep = TimeSpan.MaxValue, ItemsToKeep = int.MaxValue });

#pragma warning disable CS0618 // Type or member is obsolete
                    yield return new ICacheTestCase(new WeakCache<int, int>(new EvenOddEqual()));
#pragma warning restore CS0618 // Type or member is obsolete

                    yield return new ICacheTestCase(new Cache<int, int>(new WeakDictionaryStorage<int, BaseCachedItem<int>>(new EvenOddEqual())));
                    yield return new ICacheTestCase(new TimedCache<int, int>(new WeakDictionaryStorage<int, TimedCachedItem<int>>(new EvenOddEqual())) { TimeToKeep = TimeSpan.MaxValue });
                    yield return new ICacheTestCase(new ItemedCache<int, int>(new WeakDictionaryStorage<int, UsefulCachedItem<int>>(new EvenOddEqual())) { ItemsToKeep = int.MaxValue });
                    yield return new ICacheTestCase(new TimedItemedCache<int, int>(new WeakDictionaryStorage<int, FullyCachedItem<int>>(new EvenOddEqual())) { TimeToKeep = TimeSpan.MaxValue, ItemsToKeep = int.MaxValue });
                    break;

                case CacheKinds.Storage:
                    var storage0 = new DictionaryStorage<TKey, BaseCachedItem<TVal>>();
                    yield return new ICacheTestCase(new Cache<TKey, TVal>(storage0), storage0);

                    var storage1 = new DictionaryStorage<TKey, TimedCachedItem<TVal>>();
                    yield return new ICacheTestCase(new TimedCache<TKey, TVal>(storage1), storage1);

                    var storage2 = new DictionaryStorage<TKey, UsefulCachedItem<TVal>>();
                    yield return new ICacheTestCase(new ItemedCache<TKey, TVal>(storage2), storage2);

                    var storage3 = new DictionaryStorage<TKey, FullyCachedItem<TVal>>();
                    yield return new ICacheTestCase(new TimedItemedCache<TKey, TVal>(storage3), storage3);
                    break;
            }
        }

        public static IEnumerable<KeyValuePair<Type, Type>> GetTypes(TypeKinds types)
        {
            IEnumerable<Type> GetTypesInternal(TypeKinds typesx)
            {
                switch (typesx)
                {
                    case TypeKinds.All:
                    default:
                        yield return typeof(int);
#if HAM
                        yield return typeof(double);
                        yield return typeof(string);
                        yield return typeof(object);
#endif
                        break;
                }
            }

            foreach (var type1 in GetTypesInternal(types))
            {
                foreach (var type2 in GetTypesInternal(types))
                {
                    yield return new KeyValuePair<Type, Type>(type1, type2);
                }
            }
        }

        private class AlwaysEqual<T> : IEqualityComparer<T>
        {
            public Boolean Equals(T x, T y) => true;
            public int GetHashCode(T obj) => 0;
        }

        private class EvenOddEqual : IEqualityComparer<int>
        {
            public Boolean Equals(int x, int y) => x % 2 == y % 2;
            public int GetHashCode(int obj) => obj % 2;
        }
    }
}
