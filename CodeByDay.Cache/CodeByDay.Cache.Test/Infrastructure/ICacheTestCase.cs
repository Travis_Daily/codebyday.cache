﻿using CodeByDay.Cache.Storage;
using NUnit.Framework;

namespace CodeByDay.Cache.Test.Infrastructure
{
    public class ICacheTestCase : TestCaseData
    {
        public ICacheTestCase(ICache param) : base(param)
        {
            SetCategory(param.GetType().Name.Split('`')[0]);
        }

        public ICacheTestCase(ICache param, IStorage storage) : base(param, storage)
        {
            SetCategory(param.GetType().Name.Split('`')[0]);
        }
    }
}
