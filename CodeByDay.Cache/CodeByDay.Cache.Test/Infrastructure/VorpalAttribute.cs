﻿using NUnit.Framework;
using System;

namespace CodeByDay.Cache.Test.Infrastructure
{
    public class VorpalAttribute : TestCaseSourceAttribute
    {
        public VorpalAttribute(CacheKinds caches, Type tKey, Type tVal) : base(typeof(TestCaseProvider), nameof(TestCaseProvider.GetCachesByType), new Object[] { caches, tKey, tVal }) { }

        public VorpalAttribute(CacheKinds caches, TypeKinds types) : base(typeof(TestCaseProvider), nameof(TestCaseProvider.GetCachesByTypeKinds), new Object[] { caches, types }) { }
    }
}
