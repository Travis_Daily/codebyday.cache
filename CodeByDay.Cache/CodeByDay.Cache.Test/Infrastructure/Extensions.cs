﻿using System;
using System.Diagnostics;

namespace CodeByDay.Cache.Test.Infrastructure
{
    public static class Extensions
    {
        public static double FullElapsedMilliseconds(this Stopwatch @this)
        {
            return @this.ElapsedTicks * 1000.0 / Stopwatch.Frequency;
        }

        public static T To<T>(this int i)
        {
            return (T)Convert.ChangeType(i, typeof(T));
        }
    }
}
