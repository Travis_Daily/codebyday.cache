﻿namespace CodeByDay.Cache.Test.Infrastructure
{
    public enum CacheKinds
    {
        Weak,
        Strong,
        All,
        Itemed,
        Timed,
        AlwaysEqual,
        EvenOddEqual,
        Storage
    }
}
