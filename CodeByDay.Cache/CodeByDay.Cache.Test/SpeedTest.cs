﻿using CodeByDay.Cache.Test.Infrastructure;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace CodeByDay.Cache.Test
{
    public abstract class SpeedTest
    {
        public const double STORAGE_SPEED_TIME = 0.0012;

        public const double ACCESS_SPEED_TIME = 0.0005;

        public const double TRY_GET_SPEED_TIME = 0.0003;

        public const double INVALIDATE_SPEED_TIME = 0.00068;

        public const double ENUMERABLE_INVALIDATE_SPEED_TIME = 0.00022;

        public const double ALL_INVALIDATE_SPEED_TIME = 0.006;

        private void Wiggle(double compare, Action<int> mainLoop)
        {
            Wiggle(compare, null, mainLoop);
        }

        private void Wiggle(double compare, Action<int> prepare, Action<int> mainLoop)
        {
#if DEBUG
            compare *= 10;
#endif

            var iterations = TestCaseProvider.SPEED_START_ITERATIONS;
            Stopwatch sw = null;

            while (iterations <= TestCaseProvider.SPEED_END_ITERATIONS)
            {
                prepare?.Invoke(iterations);

                GC.Collect();

                sw = Stopwatch.StartNew();

                mainLoop(iterations);

                sw.Stop();

                if ((sw.FullElapsedMilliseconds() / iterations <= compare))
                {
                    return;
                }

                iterations *= 10;
            }

            Assert.LessOrEqual(sw.FullElapsedMilliseconds() / iterations, compare);
        }

        [Vorpal(CacheKinds.Strong, TypeKinds.All)]
        public void ValueStorageSpeed<TKey, TVal>(ICache<TKey, TVal> cache)
        {
            Wiggle(STORAGE_SPEED_TIME,
                iterations =>
                {
                    for (int i = 0; i < iterations; i++)
                    {
                        cache.Get(i.To<TKey>(), () => i.To<TVal>());
                    }
                });
        }

        [Vorpal(CacheKinds.Strong, TypeKinds.All)]
        public void ValueAccessSpeed<TKey, TVal>(ICache<TKey, TVal> cache)
        {
            Wiggle(ACCESS_SPEED_TIME,
                iterations =>
                {
                    for (int i = 0; i < iterations; i++)
                    {
                        cache.Get(i.To<TKey>(), () => i.To<TVal>());
                    }
                },
                iterations =>
                {
                    for (int i = 0; i < iterations; i++)
                    {
                        cache.Get(i.To<TKey>(), () => i.To<TVal>());
                    }
                });
        }

        [Vorpal(CacheKinds.Strong, TypeKinds.All)]
        public void ValueInvalidateSpeed<TKey, TVal>(ICache<TKey, TVal> cache)
        {
            Wiggle(INVALIDATE_SPEED_TIME,
                iterations =>
                {
                    for (int i = 0; i < iterations; i++)
                    {
                        cache.Get(i.To<TKey>(), () => i.To<TVal>());
                    }
                },
                iterations =>
                {
                    for (int i = 0; i < iterations; i++)
                    {
                        cache.Remove(i.To<TKey>());
                    }
                });
        }

        [Vorpal(CacheKinds.Strong, TypeKinds.All)]
        public void EnumerableInvalidateSpeed<TKey, TVal>(ICache<TKey, TVal> cache)
        {
            Wiggle(ENUMERABLE_INVALIDATE_SPEED_TIME,
                iterations =>
                {
                    for (int i = 0; i < iterations; i++)
                    {
                        cache.Get(i.To<TKey>(), () => i.To<TVal>());
                    }
                },
                iterations =>
                {
                    IEnumerable<TKey> ToEnumerable()
                    {
                        for (int i = 0; i < iterations; i++)
                        {
                            yield return i.To<TKey>();
                        }
                    }

                    cache.Remove(ToEnumerable());
                });
        }

        [Vorpal(CacheKinds.Strong, TypeKinds.All)]
        public void AllInvalidateSpeed<TKey, TVal>(ICache<TKey, TVal> cache)
        {
            var sw = new Stopwatch();

            for (int i = 0; i < TestCaseProvider.ALL_INVALIDATE_SPEED_ITERATIONS; i++)
            {
                cache.Get(i.To<TKey>(), () => i.To<TVal>());
            }

            GC.Collect();

            sw.Start();

            cache.Clear();

            sw.Stop();

            Assert.LessOrEqual(sw.FullElapsedMilliseconds() / TestCaseProvider.ALL_INVALIDATE_SPEED_ITERATIONS, ALL_INVALIDATE_SPEED_TIME);
        }

        [Vorpal(CacheKinds.Strong, TypeKinds.All)]
        public void ValueTryGetSpeed<TKey, TVal>(ICache<TKey, TVal> cache)
        {
            Wiggle(TRY_GET_SPEED_TIME,
                iterations =>
                {
                    for (int i = 0; i < iterations; i++)
                    {
                        cache.Get(i.To<TKey>(), () => i.To<TVal>());
                    }
                },
                iterations =>
                {
                    for (int i = 0; i < iterations; i++)
                    {
                        cache.TryGet(i.To<TKey>(), out var outValue);
                    }
                });
        }

        [Vorpal(CacheKinds.Strong, TypeKinds.All)]
        public void ValueTryGetFailSpeed<TKey, TVal>(ICache<TKey, TVal> cache)
        {
            Wiggle(TRY_GET_SPEED_TIME,
                iterations =>
                {
                    for (int i = 0; i < iterations; i++)
                    {
                        cache.TryGet(i.To<TKey>(), out var outValue);
                    }
                }
            );
        }
    }
}
