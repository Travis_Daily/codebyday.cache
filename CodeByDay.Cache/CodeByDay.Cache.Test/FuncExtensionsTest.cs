﻿using CodeByDay.Cache.Extensions;
using NUnit.Framework;
using System;

namespace CodeByDay.Cache.Test
{
    public abstract class FuncExtensionsTest
    {
        [TestCase(Category = nameof(FuncExtensions))]
        public void SingleExecute()
        {
            var testValue = 0;
            Func<int> func = () =>
            {
                testValue++;
                return 0;
            };

            Assert.AreEqual(0, func.ExecuteCached());
            Assert.AreEqual(0, func.ExecuteCached());

            Assert.AreEqual(1, testValue);
        }

        [TestCase(Category = nameof(FuncExtensions))]
        public void Invalidate()
        {
            var testValue = 0;
            Func<int> func = () =>
            {
                testValue++;
                return 0;
            };

            Assert.AreEqual(0, func.ExecuteCached());

            FuncExtensions.Clear();

            Assert.AreEqual(0, func.ExecuteCached());

            Assert.AreEqual(2, testValue);
        }

        [TestCase(Category = nameof(FuncExtensions))]
        public void SingleExecuteParam()
        {
            var testValue = 0;
            Func<int, int> func = input =>
            {
                testValue++;
                return input;
            };

            Assert.AreEqual(0, func.ExecuteCached(0));
            Assert.AreEqual(0, func.ExecuteCached(0));

            Assert.AreEqual(1, testValue);
        }

        [TestCase(Category = nameof(FuncExtensions))]
        public void MultiCacheParam()
        {
            var testValue = 0;
            Func<int, int> func = input =>
            {
                testValue++;
                return input;
            };

            Assert.AreEqual(0, func.ExecuteCached(0));
            Assert.AreEqual(1, func.ExecuteCached(1));

            Assert.AreEqual(2, testValue);
        }

        [TestCase(Category = nameof(FuncExtensions))]
        public void InvalidateParam()
        {
            var testValue = 0;
            Func<int, int> func = input =>
            {
                testValue++;
                return input;
            };

            Assert.AreEqual(0, func.ExecuteCached(0));

            FuncExtensions.Clear();

            Assert.AreEqual(0, func.ExecuteCached(0));

            Assert.AreEqual(2, testValue);
        }

        [TestCase(Category = nameof(FuncExtensions))]
        public void SingleExecuteParams()
        {
            var testValue = 0;
            Func<int, int, int> func = (input1, input2) =>
            {
                testValue++;
                return input1 + input2;
            };

            Assert.AreEqual(0, func.ExecuteCached(0, 0));
            Assert.AreEqual(0, func.ExecuteCached(0, 0));

            Assert.AreEqual(1, testValue);
        }

        [TestCase(Category = nameof(FuncExtensions))]
        public void MultiCacheParams()
        {
            var testValue = 0;
            Func<int, int, int> func = (input1, input2) =>
            {
                testValue++;
                return input1 + input2;
            };

            Assert.AreEqual(0, func.ExecuteCached(0, 0));
            Assert.AreEqual(2, func.ExecuteCached(1, 1));

            Assert.AreEqual(2, testValue);
        }

        [TestCase(Category = nameof(FuncExtensions))]
        public void InvalidateParams()
        {
            var testValue = 0;
            Func<int, int, int> func = (input1, input2) =>
            {
                testValue++;
                return input1 + input2;
            };

            Assert.AreEqual(0, func.ExecuteCached(0, 0));

            FuncExtensions.Clear();

            Assert.AreEqual(0, func.ExecuteCached(0, 0));

            Assert.AreEqual(2, testValue);
        }

        [TestCase(Category = nameof(FuncExtensions))]
        public void SingleExecuteAllParams()
        {
            var testValue = 0;
            Func<int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int> func = (input1, input2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13, i14, i15, i16) =>
            {
                testValue++;
                return input1 + input2;
            };

            Assert.AreEqual(0, func.ExecuteCached(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
            Assert.AreEqual(0, func.ExecuteCached(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));

            Assert.AreEqual(1, testValue);
        }

        [TestCase(Category = nameof(FuncExtensions))]
        public void MultiCacheAllParams()
        {
            var testValue = 0;
            Func<int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int> func = (input1, input2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13, i14, i15, i16) =>
            {
                testValue++;
                return input1 + input2;
            };

            Assert.AreEqual(0, func.ExecuteCached(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
            Assert.AreEqual(2, func.ExecuteCached(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1));

            Assert.AreEqual(2, testValue);
        }

        [TestCase(Category = nameof(FuncExtensions))]
        public void InvalidateAllParams()
        {
            var testValue = 0;
            Func<int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int> func = (input1, input2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13, i14, i15, i16) =>
            {
                testValue++;
                return input1 + input2;
            };

            Assert.AreEqual(0, func.ExecuteCached(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));

            FuncExtensions.Clear();

            Assert.AreEqual(0, func.ExecuteCached(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));

            Assert.AreEqual(2, testValue);
        }
    }
}
