﻿using CodeByDay.Cache.Test.Infrastructure;
using NUnit.Framework;
using System;

namespace CodeByDay.Cache.Test
{
    public abstract class WeakCacheTest
    {
        [Vorpal(CacheKinds.Weak, TypeKinds.All)]
        public void ValueDereference<TKey, TVal>(ICache<TKey, TVal> cache)
        {
            // Force garbage collection so the weak caches don't fail.
            GC.Collect();

            // Test that the first run produced the value from the function.
            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual(i.To<TVal>(), cache.Get(i.To<TKey>(), () => i.To<TVal>()));
            }

            GC.Collect();

            // Test that further runs after a garbage collect have no cached value.
            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual((-1).To<TVal>(), cache.Get(i.To<TKey>(), () => (-1).To<TVal>()));
            }
        }
    }
}
