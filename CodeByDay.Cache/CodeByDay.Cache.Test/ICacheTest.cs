﻿using CodeByDay.Cache.Storage;
using CodeByDay.Cache.Test.Infrastructure;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CodeByDay.Cache.Test
{
    public abstract class ICacheTest
    {
        [Vorpal(CacheKinds.All, TypeKinds.All)]
        public void ValueStorage<TKey, TVal>(ICache<TKey, TVal> cache)
        {
            if (cache is ITestInterfaces u && u.StorageIsWeak())
            {
                // Force garbage collection so the weak caches don't fail.
                GC.Collect();
            }

            // Test that the first run produced the value from the function.
            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual(i.To<TVal>(), cache.Get(i.To<TKey>(), () => i.To<TVal>()));
            }

            // Test that further runs produce the cached value.
            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual(i.To<TVal>(), cache.Get(i.To<TKey>(), () => (-1).To<TVal>()));
            }
        }

        [Vorpal(CacheKinds.All, TypeKinds.All)]
        public async Task ValueStorageAsync<TKey, TVal>(ICache<TKey, TVal> cache)
        {
            if (cache is ITestInterfaces u && u.StorageIsWeak())
            {
                // Force garbage collection so the weak caches don't fail.
                GC.Collect();
            }

            // Test that the first run produced the value from the function.
            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual(i.To<TVal>(), await cache.GetAsync(i.To<TKey>(), () => Task.FromResult(i.To<TVal>())));
            }

            // Test that further runs produce the cached value.
            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual(i.To<TVal>(), await cache.GetAsync(i.To<TKey>(), () => Task.FromResult((-1).To<TVal>())));
            }
        }

        [Vorpal(CacheKinds.All, TypeKinds.All)]
        public void ValueInvalidate<TKey, TVal>(ICache<TKey, TVal> cache)
        {
            if (cache is ITestInterfaces u && u.StorageIsWeak())
            {
                // Force garbage collection so the weak caches don't fail.
                GC.Collect();
            }

            // Set inital values.
            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                cache.Get(i.To<TKey>(), () => i.To<TVal>());
            }

            // Invalidate each.
            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                cache.Remove(i.To<TKey>());
            }

            // Check that the old values were invalidated and that the function is being ran.
            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual((-1).To<TVal>(), cache.Get(i.To<TKey>(), () => (-1).To<TVal>()));
            }
        }

        [Vorpal(CacheKinds.All, TypeKinds.All)]
        public void EnumerableInvalidate<TKey, TVal>(ICache<TKey, TVal> cache)
        {
            if (cache is ITestInterfaces u && u.StorageIsWeak())
            {
                // Force garbage collection so the weak caches don't fail.
                GC.Collect();
            }

            // Set inital values.
            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                cache.Get(i.To<TKey>(), () => i.To<TVal>());
            }

            // Invalidate via enumerable.
            cache.Remove(ToEnumerable());
            IEnumerable<TKey> ToEnumerable()
            {
                for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
                {
                    yield return i.To<TKey>();
                }
            }

            // Check that the old values were invalidated and that the function is being ran.
            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual((-1).To<TVal>(), cache.Get(i.To<TKey>(), () => (-1).To<TVal>()));
            }
        }

        [Vorpal(CacheKinds.All, TypeKinds.All)]
        public void AllInvalidate<TKey, TVal>(ICache<TKey, TVal> cache)
        {
            if (cache is ITestInterfaces u && u.StorageIsWeak())
            {
                // Force garbage collection so the weak caches don't fail.
                GC.Collect();
            }

            // Set inital values.
            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                cache.Get(i.To<TKey>(), () => i.To<TVal>());
            }

            // Invalidate.
            cache.Clear();

            // Check that the old values were invalidated and that the function is being ran.
            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual((-1).To<TVal>(), cache.Get(i.To<TKey>(), () => (-1).To<TVal>()));
            }
        }

        [Vorpal(CacheKinds.All, TypeKinds.All)]
        public void ValueTryGet<TKey, TVal>(ICache<TKey, TVal> cache)
        {
            if (cache is ITestInterfaces u && u.StorageIsWeak())
            {
                // Force garbage collection so the weak caches don't fail.
                GC.Collect();
            }

            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                // Assert that there is no existing value.
                Assert.IsFalse(cache.TryGet(i.To<TKey>(), out TVal outValue));

                cache.Get(i.To<TKey>(), () => i.To<TVal>());

                // Assert that a value now exists.
                Assert.IsTrue(cache.TryGet(i.To<TKey>(), out outValue));

                // Make sure that value is the expected value.
                Assert.AreEqual(i.To<TVal>(), outValue);
            }
        }

        [Vorpal(CacheKinds.Storage, TypeKinds.All)]
        public void ValueDirectClear<TKey, TVal>(ICache<TKey, TVal> cache, IStorage storage)
        {
            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual(i.To<TVal>(), cache.Get(i.To<TKey>(), () => i.To<TVal>()));
            }

            storage.Clear();

            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual((-1).To<TVal>(), cache.Get(i.To<TKey>(), () => (-1).To<TVal>()));
            }
        }

        private class KeyVal
        {
            public int Key;
            public long Val;
        }

        [Vorpal(CacheKinds.All, typeof(int), typeof(long))]
        public void RecursiveCall(ICache<int, long> cache)
        {
            if (cache is ITestInterfaces u && u.StorageIsWeak())
            {
                // Force garbage collection so the weak caches don't fail.
                GC.Collect();
            }

            IEnumerable<KeyVal> Params()
            {
                yield return new KeyVal { Key = 1, Val = 1 };
                yield return new KeyVal { Key = 2, Val = 1 };
                yield return new KeyVal { Key = 3, Val = 1 };
                yield return new KeyVal { Key = 4, Val = 3 };
                yield return new KeyVal { Key = 5, Val = 5 };
                yield return new KeyVal { Key = 6, Val = 9 };
                yield return new KeyVal { Key = 7, Val = 17 };
                yield return new KeyVal { Key = 8, Val = 31 };
                yield return new KeyVal { Key = 9, Val = 57 };
                yield return new KeyVal { Key = 10, Val = 105 };
                yield return new KeyVal { Key = 11, Val = 193 };
                yield return new KeyVal { Key = 12, Val = 355 };
                yield return new KeyVal { Key = 13, Val = 653 };
                yield return new KeyVal { Key = 14, Val = 1201 };
                yield return new KeyVal { Key = 15, Val = 2209 };
                yield return new KeyVal { Key = 16, Val = 4063 };
                yield return new KeyVal { Key = 17, Val = 7473 };
                yield return new KeyVal { Key = 18, Val = 13745 };
                yield return new KeyVal { Key = 19, Val = 25281 };
                yield return new KeyVal { Key = 20, Val = 46499 };
                yield return new KeyVal { Key = 21, Val = 85525 };
                yield return new KeyVal { Key = 22, Val = 157305 };
                yield return new KeyVal { Key = 23, Val = 289329 };
                yield return new KeyVal { Key = 24, Val = 532159 };
                yield return new KeyVal { Key = 25, Val = 978793 };
                yield return new KeyVal { Key = 26, Val = 1800281 };
                yield return new KeyVal { Key = 27, Val = 3311233 };
                yield return new KeyVal { Key = 28, Val = 6090307 };
                yield return new KeyVal { Key = 29, Val = 11201821 };
                yield return new KeyVal { Key = 30, Val = 20603361 };
                yield return new KeyVal { Key = 31, Val = 37895489 };
                yield return new KeyVal { Key = 32, Val = 69700671 };
                yield return new KeyVal { Key = 33, Val = 128199521 };
                yield return new KeyVal { Key = 34, Val = 235795681 };
                yield return new KeyVal { Key = 35, Val = 433695873 };
                yield return new KeyVal { Key = 36, Val = 797691075 };
                yield return new KeyVal { Key = 37, Val = 1467182629 };
                yield return new KeyVal { Key = 38, Val = 2698569577 };
                yield return new KeyVal { Key = 39, Val = 4963443281 };
                yield return new KeyVal { Key = 40, Val = 9129195487 };
                yield return new KeyVal { Key = 41, Val = 16791208345 };
                yield return new KeyVal { Key = 42, Val = 30883847113 };
                yield return new KeyVal { Key = 43, Val = 56804250945 };
                yield return new KeyVal { Key = 44, Val = 104479306403 };
                yield return new KeyVal { Key = 45, Val = 192167404461 };
                yield return new KeyVal { Key = 46, Val = 353450961809 };
                yield return new KeyVal { Key = 47, Val = 650097672673 };
                yield return new KeyVal { Key = 48, Val = 1195716038943 };
                yield return new KeyVal { Key = 49, Val = 2199264673425 };
                yield return new KeyVal { Key = 50, Val = 4045078385041 };
                yield return new KeyVal { Key = 51, Val = 7440059097409 };
                yield return new KeyVal { Key = 52, Val = 13684402155875 };
                yield return new KeyVal { Key = 53, Val = 25169539638325 };
                yield return new KeyVal { Key = 54, Val = 46294000891609 };
                yield return new KeyVal { Key = 55, Val = 85147942685809 };
                yield return new KeyVal { Key = 56, Val = 156611483215743 };
                yield return new KeyVal { Key = 57, Val = 288053426793161 };
                yield return new KeyVal { Key = 58, Val = 529812852694713 };
                yield return new KeyVal { Key = 59, Val = 974477762703617 };
                yield return new KeyVal { Key = 60, Val = 1792344042191491 };
                yield return new KeyVal { Key = 61, Val = 3296634657589821 };
                yield return new KeyVal { Key = 62, Val = 6063456462484929 };
                yield return new KeyVal { Key = 63, Val = 11152435162266241 };
                yield return new KeyVal { Key = 64, Val = 20512526282340991 };
                yield return new KeyVal { Key = 65, Val = 37728417907092161 };
                yield return new KeyVal { Key = 66, Val = 69393379351699393 };
                yield return new KeyVal { Key = 67, Val = 127634323541132545 };
                yield return new KeyVal { Key = 68, Val = 234756120799924099 };
                yield return new KeyVal { Key = 69, Val = 431783823692756037 };
                yield return new KeyVal { Key = 70, Val = 794174268033812681 };
                yield return new KeyVal { Key = 71, Val = 1460714212526492817 };
                yield return new KeyVal { Key = 72, Val = 2686672304253061535 };
                yield return new KeyVal { Key = 73, Val = 4941560784813367033 };
                yield return new KeyVal { Key = 74, Val = 9088947301592921385 };
            };

            foreach (var kv in Params())
            {
                Assert.AreEqual(kv.Val, cache.Get(kv.Key, () => RecusiveFunction(kv.Key, cache)));
            }

            long RecusiveFunction(int index, ICache<int, long> xcache)
            {
                if (index <= 0)
                {
                    throw new Exception();
                }
                else if (index < 4)
                {
                    return 1;
                }
                else
                {
                    return xcache.Get(index - 1, () => RecusiveFunction(index - 1, xcache)) +
                        xcache.Get(index - 2, () => RecusiveFunction(index - 2, xcache)) +
                        xcache.Get(index - 3, () => RecusiveFunction(index - 3, xcache));
                }
            }
        }
    }
}
