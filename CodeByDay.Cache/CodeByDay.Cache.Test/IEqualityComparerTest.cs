﻿using CodeByDay.Cache.Test.Infrastructure;
using NUnit.Framework;
using System;

namespace CodeByDay.Cache.Test
{
    public abstract class IEqualityComparerTest
    {
        [Vorpal(CacheKinds.AlwaysEqual, TypeKinds.All)]
        public void AlwaysEqual<TKey, TVal>(ICache<TKey, TVal> cache)
        {
            if (cache is ITestInterfaces u && u.StorageIsWeak())
            {
                // Force garbage collection so the weak caches don't fail.
                GC.Collect();
            }

            // Test that the first run produced the value from the function.
            Assert.AreEqual(0.To<TVal>(), cache.Get(0.To<TKey>(), () => 0.To<TVal>()));

            // Test that any key produces the cached value.
            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual(0.To<TVal>(), cache.Get(i.To<TKey>(), () => i.To<TVal>()));
            }
        }

        [Vorpal(CacheKinds.EvenOddEqual, typeof(int), typeof(int))]
        public void EvenOddEqual(ICache<int, int> cache)
        {
            if (cache is ITestInterfaces u && u.StorageIsWeak())
            {
                // Force garbage collection so the weak caches don't fail.
                GC.Collect();
            }

            // Test that the first run produced the value from the function.
            Assert.AreEqual(0, cache.Get(0, () => 0));
            Assert.AreEqual(1, cache.Get(1, () => 1));

            // Test that any key produces the cached value.
            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual(i % 2, cache.Get(i, () => i));
            }
        }
    }
}
