﻿using CodeByDay.Cache.CachedItem;
using CodeByDay.Cache.Storage;
using CodeByDay.Cache.Test.Infrastructure;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CodeByDay.Cache.Test
{
    public abstract class ITimedCacheTest
    {

        [Vorpal(CacheKinds.Timed, TypeKinds.All)]
        public void TimedValueStorage<TKey, TVal>(ITimedCache<TKey, TVal> cache)
        {
            if (cache is ITestInterfaces u && u.StorageIsWeak())
            {
                // Force garbage collection so the weak caches don't fail.
                GC.Collect();
            }

            // Test that the first run produced the value from the function.
            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual(i.To<TVal>(), cache.Get(i.To<TKey>(), TimeSpan.FromDays(1), () => i.To<TVal>()));
            }

            // Test that further runs produce the cached value.
            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual(i.To<TVal>(), cache.Get(i.To<TKey>(), TimeSpan.FromDays(1), () => (-1).To<TVal>()));
            }
        }

        [Vorpal(CacheKinds.Timed, TypeKinds.All)]
        public async Task TimedValueStorageAsync<TKey, TVal>(ITimedCache<TKey, TVal> cache)
        {
            if (cache is ITestInterfaces u && u.StorageIsWeak())
            {
                // Force garbage collection so the weak caches don't fail.
                GC.Collect();
            }

            // Test that the first run produced the value from the function.
            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual(i.To<TVal>(), await cache.GetAsync(i.To<TKey>(), TimeSpan.FromDays(1), () => Task.FromResult(i.To<TVal>())));
            }

            // Test that further runs produce the cached value.
            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual(i.To<TVal>(), await cache.GetAsync(i.To<TKey>(), TimeSpan.FromDays(1), () => Task.FromResult((-1).To<TVal>())));
            }
        }

        [Vorpal(CacheKinds.Timed, TypeKinds.All)]
        public void TimedValueKeyValuePairStorage<TKey, TVal>(ITimedCache<TKey, TVal> cache)
        {
            if (cache is ITestInterfaces u && u.StorageIsWeak())
            {
                // Force garbage collection so the weak caches don't fail.
                GC.Collect();
            }

            // Test that the first run produced the value from the function.
            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual(i.To<TVal>(), cache.Get(i.To<TKey>(), () => new KeyValuePair<TimeSpan, TVal>(TimeSpan.FromDays(1), i.To<TVal>())));
            }

            // Test that further runs produce the cached value.
            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual(i.To<TVal>(), cache.Get(i.To<TKey>(), () => new KeyValuePair<TimeSpan, TVal>(TimeSpan.FromDays(1), (-1).To<TVal>())));
            }
        }

        [Vorpal(CacheKinds.Timed, TypeKinds.All)]
        public async Task TimedValueKeyValuePairStorageAsync<TKey, TVal>(ITimedCache<TKey, TVal> cache)
        {
            if (cache is ITestInterfaces u && u.StorageIsWeak())
            {
                // Force garbage collection so the weak caches don't fail.
                GC.Collect();
            }

            // Test that the first run produced the value from the function.
            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual(i.To<TVal>(), await cache.GetAsync(i.To<TKey>(), () => Task.FromResult(new KeyValuePair<TimeSpan, TVal>(TimeSpan.FromDays(1), i.To<TVal>()))));
            }

            // Test that further runs produce the cached value.
            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual(i.To<TVal>(), await cache.GetAsync(i.To<TKey>(), () => Task.FromResult(new KeyValuePair<TimeSpan, TVal>(TimeSpan.FromDays(1), (-1).To<TVal>()))));
            }
        }

        [Vorpal(CacheKinds.Timed, TypeKinds.All)]
        public void TimedValueValueTupleStorage<TKey, TVal>(ITimedCache<TKey, TVal> cache)
        {
            if (cache is ITestInterfaces u && u.StorageIsWeak())
            {
                // Force garbage collection so the weak caches don't fail.
                GC.Collect();
            }

            // Test that the first run produced the value from the function.
            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual(i.To<TVal>(), cache.Get(i.To<TKey>(), () => (TimeSpan.FromDays(1), i.To<TVal>())));
            }

            // Test that further runs produce the cached value.
            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual(i.To<TVal>(), cache.Get(i.To<TKey>(), () => (TimeSpan.FromDays(1), (-1).To<TVal>())));
            }
        }

        [Vorpal(CacheKinds.Timed, TypeKinds.All)]
        public async Task TimedValueValueTupleStorageAsync<TKey, TVal>(ITimedCache<TKey, TVal> cache)
        {
            if (cache is ITestInterfaces u && u.StorageIsWeak())
            {
                // Force garbage collection so the weak caches don't fail.
                GC.Collect();
            }

            // Test that the first run produced the value from the function.
            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual(i.To<TVal>(), await cache.GetAsync(i.To<TKey>(), () => Task.FromResult((TimeSpan.FromDays(1), i.To<TVal>()))));
            }

            // Test that further runs produce the cached value.
            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual(i.To<TVal>(), await cache.GetAsync(i.To<TKey>(), () => Task.FromResult((TimeSpan.FromDays(1), (-1).To<TVal>()))));
            }
        }

        [Vorpal(CacheKinds.Timed, TypeKinds.All)]
        public void TimedKeepAndRemoval<TKey, TVal>(ITimedCache<TKey, TVal> cache)
        {
            // Set the values from 0 to BISECTION_POINT.
            for (int i = 0; i < TestCaseProvider.BISECTION_POINT; i++)
            {
                cache.Get(i.To<TKey>(), () => i.To<TVal>());
            }

            var sw = Stopwatch.StartNew();

            Thread.Sleep(TestCaseProvider.TIME_TO_KEEP);

            // Set the remaining values.
            for (int i = TestCaseProvider.BISECTION_POINT; i < TestCaseProvider.ITERATIONS; i++)
            {
                cache.Get(i.To<TKey>(), () => i.To<TVal>());
            }

            var failures = new StringBuilder();

            // Assert that values from 0 to BISECTION_POINT no longer exist.
            for (int i = 0; i < TestCaseProvider.BISECTION_POINT; i++)
            {
                if (!(-1).To<TVal>().Equals(cache.Get(i.To<TKey>(), () => (-1).To<TVal>())))
                {
                    failures.AppendLine($"{i} != -1");
                }
            }

            // Assert that values from BISECTION_POINT to ITERATIONS still exist.
            for (int i = TestCaseProvider.BISECTION_POINT; i < TestCaseProvider.ITERATIONS; i++)
            {
                if (!i.To<TVal>().Equals(cache.Get(i.To<TKey>(), () => (-1).To<TVal>())))
                {
                    failures.AppendLine($"{i} != i");
                }
            }

            sw.Stop();

            if (sw.ElapsedMilliseconds < TestCaseProvider.TIME_TO_KEEP * 2)
            {
                Assert.AreEqual("", failures.ToString());
            }
            else
            {
                // Test took too long. Try again
                cache.Clear();
                TimedKeepAndRemoval(cache);
            }
        }

        [Vorpal(CacheKinds.Timed, TypeKinds.All)]
        public void TimedVariable<TKey, TVal>(ITimedCache<TKey, TVal> cache)
        {
            var timespanToKeep = TimeSpan.FromMilliseconds(TestCaseProvider.TIME_TO_KEEP);
            Func<int, TimeSpan> msFinder = input => (input % 2 == 0 ? timespanToKeep : TimeSpan.MaxValue);

            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                cache.Get(i.To<TKey>(), msFinder(i), () => i.To<TVal>());
            }

            Thread.Sleep(TestCaseProvider.TIME_TO_KEEP);

            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                var output = cache.Get(i.To<TKey>(), () => (-1).To<TVal>());
                if (msFinder(i) <= timespanToKeep)
                {
                    Assert.AreEqual((-1).To<TVal>(), output);
                }
                else
                {
                    Assert.AreEqual((i).To<TVal>(), output);
                }
            }
        }

        [Vorpal(CacheKinds.Timed, TypeKinds.All)]
        public void TimedVariableFunc<TKey, TVal>(ITimedCache<TKey, TVal> cache)
        {
            var timespanToKeep = TimeSpan.FromMilliseconds(TestCaseProvider.TIME_TO_KEEP);
            Func<int, TimeSpan> msFinder = input => (input % 2 == 0 ? timespanToKeep : TimeSpan.MaxValue);

            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                cache.Get(i.To<TKey>(), () => new KeyValuePair<TimeSpan, TVal>(msFinder(i), i.To<TVal>()));
            }

            Thread.Sleep(TestCaseProvider.TIME_TO_KEEP);

            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                var output = cache.Get(i.To<TKey>(), () => (-1).To<TVal>());
                if (msFinder(i) <= timespanToKeep)
                {
                    Assert.AreEqual((-1).To<TVal>(), output);
                }
                else
                {
                    Assert.AreEqual((i).To<TVal>(), output);
                }
            }
        }

        [Vorpal(CacheKinds.Timed, TypeKinds.All)]
        public void TimedIncrease<TKey, TVal>(ITimedCache<TKey, TVal> cache)
        {
            cache.TimeToKeep = TimeSpan.MaxValue;

            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                cache.Get(i.To<TKey>(), () => i.To<TVal>());
            }

            Thread.Sleep(TestCaseProvider.TIME_TO_KEEP);

            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual(i.To<TVal>(), cache.Get(i.To<TKey>(), () => (-1).To<TVal>()));
            }
        }

        [Vorpal(CacheKinds.Timed, TypeKinds.All)]
        public void TimedDecrease<TKey, TVal>(ITimedCache<TKey, TVal> cache)
        {
            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                cache.Get(i.To<TKey>(), () => i.To<TVal>());
            }

            Thread.Sleep(TestCaseProvider.TIME_TO_KEEP / 10);

            cache.TimeToKeep = TimeSpan.FromMilliseconds(TestCaseProvider.TIME_TO_KEEP / 10);

            cache.Clean();

            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual((-1).To<TVal>(), cache.Get(i.To<TKey>(), () => (-1).To<TVal>()));
            }
        }

        [TestCase]
        public void InvalidReserializeTimedCache()
        {
            var isp = new DictionaryStorage<int, TimedCachedItem<int>>();

            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                isp[i] = new TimedCachedItem<int>
                {
                    CreatedTime = new DateTime(2017, 1, 1),
                    TimeToKeep = TimeSpan.FromDays(1),
                    Value = 10
                };
            }

            var cache = new TimedCache<int, int>(isp);

            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual(cache.TryGet(i, out _), false);
            }
        }

        [TestCase]
        public void InvalidReserializeTimedItemedCache()
        {
            var isp = new DictionaryStorage<int, FullyCachedItem<int>>();

            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                isp[i] = new FullyCachedItem<int>
                {
                    CreatedTime = new DateTime(2017, 1, 1),
                    TimeToKeep = TimeSpan.FromDays(1),
                    Value = 10
                };
            }

            var cache = new TimedItemedCache<int, int>(isp);

            for (int i = 0; i < TestCaseProvider.ITERATIONS; i++)
            {
                Assert.AreEqual(cache.TryGet(i, out _), false);
            }
        }
    }
}
