﻿using CodeByDay.Cache.CachedItem;
using CodeByDay.Cache.Storage;

namespace CodeByDay.Cache
{
    /// <summary>
    /// Cache that stores values which expire after an amount of time. TValues are stored in a dictionary and are retrieved by the TKey.
    /// </summary>
    /// <typeparam name="TKey">The key type. Should likely be String.</typeparam>
    /// <typeparam name="TValue">The value type.</typeparam>
    public class TimedCache<TKey, TValue> : BaseTimedCache<TKey, TValue, TimedCachedItem<TValue>>
    {
        /// <summary>
        /// Creates a new TimedCache.
        /// </summary>
        public TimedCache() : this(new DictionaryStorage<TKey, TimedCachedItem<TValue>>()) { }

        /// <summary>
        /// Creates a new TimedCache.
        /// </summary>
        /// <param name="storage">The item which maintains the internal storage.</param>
        public TimedCache(IStorage<TKey, TimedCachedItem<TValue>> storage) : base(storage) { }
    }
}
