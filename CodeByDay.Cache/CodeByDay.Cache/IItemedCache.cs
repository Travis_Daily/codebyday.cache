﻿using CodeByDay.Cache.Usefulness;

namespace CodeByDay.Cache
{
    /// <summary>
    /// Cache that only stores a given number of items.
    /// </summary>
    public interface IItemedCache : ICache, ICleanable
    {
        /// <summary>
        /// At most, this many key-values will be kept after a Clean().
        /// </summary>
        int ItemsToKeep { get; set; }

        /// <summary>
        /// An IUsefulnessProvider which is used to order the cached values during an invalidation.
        /// </summary>
        IUsefulnessProvider UsefulnessProvider { get; set; }
    }

    /// <summary>
    /// Cache that only stores a given number of items.
    /// </summary>
    public interface IItemedCache<TKey, TValue> : IItemedCache, ICache<TKey, TValue> { }
}
