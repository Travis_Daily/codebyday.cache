﻿using CodeByDay.Cache.CachedItem;
using CodeByDay.Cache.Storage;

namespace CodeByDay.Cache
{
    /// <summary>
    /// Cache to store values. TValues are stored in a dictionary and are retrieved by the TKey.
    /// </summary>
    /// <typeparam name="TKey">The key type. Should likely be String.</typeparam>
    /// <typeparam name="TValue">The value type.</typeparam>
    public class Cache<TKey, TValue> : BaseCache<TKey, TValue, BaseCachedItem<TValue>>, ICache<TKey, TValue>
    {
        /// <summary>
        /// Creates a new Cache.
        /// </summary>
        public Cache() : this(new DictionaryStorage<TKey, BaseCachedItem<TValue>>()) { }
        
        /// <summary>
        /// Creates a new Cache.
        /// </summary>
        /// <param name="storage">The item which maintains the internal storage.</param>
        public Cache(IStorage<TKey, BaseCachedItem<TValue>> storage) : base(storage) { }
    }
}
