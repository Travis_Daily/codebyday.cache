﻿using CodeByDay.Cache.CachedItem;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CodeByDay.Cache.Storage
{
    /// <summary>
    /// An IStorage which uses a Dictionary as its storage medium with weak references to the stored items.
    /// </summary>
    /// <typeparam name="TKey">The key of the IDictionary.</typeparam>
    /// <typeparam name="TValue">The value of the IDictionary.</typeparam>
    public sealed class WeakDictionaryStorage<TKey, TValue> : IStorage<TKey, TValue>
#if !RELEASE
        , IWeak
#endif
        where TValue : ICachedItem, new()
    {
        private class WeakReference<T> : WeakReference where T : ICachedItem, new()
        {
            public T GetTarget => Target is T t ? t : new T { IsValid = false };
            public WeakReference(T target) : base(target) { }
        }

        private IDictionary<TKey, WeakReference<TValue>> Storage;

        /// <inheritdoc />
        public int Count => Storage.Count;

        /// <inheritdoc />
        public TValue this[TKey key]
        {
            get
            {
                if (key == null)
                {
                    throw new ArgumentException("Key cannot be null.");
                }
                return Storage[key].GetTarget;
            }
            set
            {
                if (key == null)
                {
                    throw new ArgumentException("Key cannot be null.");
                }
                Storage[key] = new WeakReference<TValue>(value);
            }
        }

        /// <summary>
        /// Creates a new WeakDictionaryStorage with a Dictionary as internal storage.
        /// </summary>
        public WeakDictionaryStorage()
        {
            Storage = new Dictionary<TKey, WeakReference<TValue>>();
        }

        /// <summary>
        /// Creates a new WeakDictionaryStorage with a Dictionary as internal storage.
        /// </summary>
        /// <param name="capacity">A param to pass to the base dictionary constructor.</param>
        public WeakDictionaryStorage(int capacity)
        {
            Storage = new Dictionary<TKey, WeakReference<TValue>>(capacity);
        }

        /// <summary>
        /// Creates a new WeakDictionaryStorage with a Dictionary as internal storage.
        /// </summary>
        /// <param name="comparer">A param to pass to the base dictionary constructor.</param>
        public WeakDictionaryStorage(IEqualityComparer<TKey> comparer)
        {
            Storage = new Dictionary<TKey, WeakReference<TValue>>(comparer);
        }

        /// <summary>
        /// Creates a new WeakDictionaryStorage with a Dictionary as internal storage.
        /// </summary>
        /// <param name="dictionary">A param to pass to the base dictionary constructor.</param>
        public WeakDictionaryStorage(IEnumerable<KeyValuePair<TKey, TValue>> dictionary)
        {
            Storage = new Dictionary<TKey, WeakReference<TValue>>(dictionary.ToDictionary(kv => kv.Key, kv => new WeakReference<TValue>(kv.Value)));
        }

        /// <summary>
        /// Creates a new WeakDictionaryStorage with a Dictionary as internal storage.
        /// </summary>
        /// <param name="capacity">A param to pass to the base dictionary constructor.</param>
        /// <param name="comparer">A param to pass to the base dictionary constructor.</param>
        public WeakDictionaryStorage(int capacity, IEqualityComparer<TKey> comparer)
        {
            Storage = new Dictionary<TKey, WeakReference<TValue>>(capacity, comparer);
        }

        /// <summary>
        /// Creates a new WeakDictionaryStorage with a Dictionary as internal storage.
        /// </summary>
        /// <param name="dictionary">A param to pass to the base dictionary constructor.</param>
        /// <param name="comparer">A param to pass to the base dictionary constructor.</param>
        public WeakDictionaryStorage(IEnumerable<KeyValuePair<TKey, TValue>> dictionary, IEqualityComparer<TKey> comparer)
        {
            Storage = new Dictionary<TKey, WeakReference<TValue>>(dictionary.ToDictionary(kv => kv.Key, kv => new WeakReference<TValue>(kv.Value)), comparer);
        }

        /// <inheritdoc />
        public void Clear()
        {
            Storage.Clear();
        }

        /// <inheritdoc />
        public bool ContainsKey(TKey key)
        {
            if (key == null)
            {
                throw new ArgumentException("Key cannot be null.");
            }
            return Storage.ContainsKey(key);
        }

        /// <inheritdoc />
        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return Storage.Select(kv => new KeyValuePair<TKey, TValue>(kv.Key, kv.Value.GetTarget)).GetEnumerator();
        }

        /// <inheritdoc />
        public bool Remove(TKey key)
        {
            if (key == null)
            {
                throw new ArgumentException("Key cannot be null.");
            }
            return Storage.Remove(key);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
