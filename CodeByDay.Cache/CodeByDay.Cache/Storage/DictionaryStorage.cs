﻿using System.Collections.Generic;

namespace CodeByDay.Cache.Storage
{
    /// <summary>
    /// An IStorage which uses a Dictionary as its storage medium.
    /// </summary>
    /// <typeparam name="TKey">The key of the IDictionary.</typeparam>
    /// <typeparam name="TValue">The value of the IDictionary.</typeparam>
    public class DictionaryStorage<TKey, TValue> : Dictionary<TKey, TValue>, IStorage<TKey, TValue>
    {
        /// <summary>
        /// Creates a new DictionaryStorage with a Dictionary as internal storage.
        /// </summary>
        public DictionaryStorage() { }

        /// <summary>
        /// Creates a new DictionaryStorage with a Dictionary as internal storage.
        /// </summary>
        /// <param name="capacity">A param to pass to the base dictionary constructor.</param>
        public DictionaryStorage(int capacity) : base(capacity) { }

        /// <summary>
        /// Creates a new DictionaryStorage with a Dictionary as internal storage.
        /// </summary>
        /// <param name="comparer">A param to pass to the base dictionary constructor.</param>
        public DictionaryStorage(IEqualityComparer<TKey> comparer) : base(comparer) { }

        /// <summary>
        /// Creates a new DictionaryStorage with a Dictionary as internal storage.
        /// </summary>
        /// <param name="dictionary">A param to pass to the base dictionary constructor.</param>
        public DictionaryStorage(IDictionary<TKey, TValue> dictionary) : base(dictionary) { }

        /// <summary>
        /// Creates a new DictionaryStorage with a Dictionary as internal storage.
        /// </summary>
        /// <param name="capacity">A param to pass to the base dictionary constructor.</param>
        /// <param name="comparer">A param to pass to the base dictionary constructor.</param>
        public DictionaryStorage(int capacity, IEqualityComparer<TKey> comparer) : base(capacity, comparer) { }

        /// <summary>
        /// Creates a new DictionaryStorage with a Dictionary as internal storage.
        /// </summary>
        /// <param name="dictionary">A param to pass to the base dictionary constructor.</param>
        /// <param name="comparer">A param to pass to the base dictionary constructor.</param>
        public DictionaryStorage(IDictionary<TKey, TValue> dictionary, IEqualityComparer<TKey> comparer) : base(dictionary, comparer) { }
    }
}
