﻿using System.Collections;
using System.Collections.Generic;

namespace CodeByDay.Cache.Storage
{
    /// <summary>
    /// The items required to propery store cached values.
    /// </summary>
    public interface IStorage : IEnumerable
    {
        /// <summary>
        /// Gets the number of items currently stored.
        /// </summary>
        int Count { get; }

        /// <summary>
        /// Removes all the stored items.
        /// </summary>
        void Clear();
    }

    /// <summary>
    /// The items required to propery store cached values.
    /// </summary>
    /// <typeparam name="TKey">The type of the key used to look up entries.</typeparam>
    public interface IStorage<TKey> : IStorage
    {
        /// <summary>
        /// Returns true if the given key is stored.
        /// </summary>
        bool ContainsKey(TKey key);

        /// <summary>
        /// Removes the given key from storage.
        /// </summary>
        bool Remove(TKey key);
    }

    /// <summary>
    /// The items required to propery store cached values.
    /// </summary>
    /// <typeparam name="TKey">The type of the key used to look up entries.</typeparam>
    /// <typeparam name="TValue">The type of the item to store.</typeparam>
    public interface IStorage<TKey, TValue> : IStorage<TKey>, IEnumerable<KeyValuePair<TKey, TValue>>
    {
        /// <summary>
        /// Get: Returns the given stored key's value. Throws if the key is missing.
        /// Set: Adds or updates the given key's storage entry to be the given setting item.
        /// </summary>
        TValue this[TKey key] { get; set; }
    }
}
