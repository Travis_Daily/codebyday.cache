﻿using CodeByDay.Cache.CachedItem;
using CodeByDay.Cache.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodeByDay.Cache
{
    /// <summary>
    /// Cache to store values. TValues are stored in a dictionary and are retrieved by the TKey.
    /// This object should not be created directly. Use the Cache object.
    /// </summary>
    /// <typeparam name="TKey">The key type. Should likely be String.</typeparam>
    /// <typeparam name="TValue">The value type.</typeparam>
    /// <typeparam name="TCachedItem">The kind of ICachedItem to store.</typeparam>
    public abstract class BaseCache<TKey, TValue, TCachedItem> : ICache<TKey, TValue>, ICleanable
#if !RELEASE
        , ITestInterfaces
#endif
        where TCachedItem : ICachedItem<TValue>, new()
    {
        /// <summary>
        /// Dictionary storage of Keys to CachedItems.
        /// </summary>
        protected IStorage<TKey, TCachedItem> Storage { get; private set; }

#if !RELEASE
        /// <summary>
        /// INTERNAL; FOR TESTING: Returns if this ICache has a weak IStorage.
        /// </summary>
        public bool StorageIsWeak() => Storage is IWeak;
#endif

        /// <summary>
        /// This Object MUST be locked on when accessing the Storage.
        /// </summary>
        protected readonly Object storageLock = new Object();

        /// <summary>
        /// A Semaphore used to prevent multiple Clean()s from running simultaniously.
        /// </summary>
        protected int CleansSemaphore = 0;

#if !RELEASE
        /// <summary>
        /// INTERNAL; FOR TESTING: Returns true if a clean is running.
        /// </summary>
        public bool CleanIsRunning() => CleansSemaphore > 0;
#endif

        /// <summary>
        /// Creates a new BaseCache.
        /// </summary>
        protected BaseCache(IStorage<TKey, TCachedItem> storage)
        {
            Storage = storage;
        }

        /// <summary>
        /// Returns true if this cache needs to be cleaned.
        /// </summary>
        protected virtual Boolean NeedsClean() => false;

        /// <summary>
        /// Returns if the TKey exists in the dictionary and is valid.
        /// </summary>
        /// <param name="key">The key to look for.</param>
        /// <returns>True if the value exists and is valid.</returns>
        protected virtual bool HasValue(TKey key)
        {
            lock (storageLock)
            {
                if (!Storage.ContainsKey(key))
                {
                    return false;
                }

                return Storage[key].IsValid;
            }
        }

        /// <see cref="ICache{TKey, TValue}.TryGet"/>
        public bool TryGet(TKey key, out TValue value)
        {
            lock (storageLock)
            {
                if (HasValue(key))
                {
                    var stored = Storage[key];
                    value = stored.Value;

                    if (stored.IsValid)
                    {
                        return true;
                    }
                }
            }

            value = default(TValue);
            return false;
        }

        /// <see cref="ICache{TKey, TValue}.Get"/>
        public TValue Get(TKey key, Func<TValue> populatorFunc)
        {
            TValue value;
            lock (storageLock)
            {
                if (!TryGet(key, out value))
                {
                    value = populatorFunc();
                    Storage[key] = new TCachedItem { Value = value };
                }
            }

            if (CleansSemaphore <= 0 && NeedsClean())
            {
                CleansSemaphore++;
                Task.Run(() => Clean()).ContinueWith(t => { CleansSemaphore--; });
            }

            return value;
        }

        private System.Threading.SemaphoreSlim AsyncSemaphore { get; } = new System.Threading.SemaphoreSlim(1, 1);

        /// <see cref="ICache{TKey, TValue}.GetAsync"/>
        public async Task<TValue> GetAsync(TKey key, Func<Task<TValue>> populatorFunc)
        {
            if (!TryGet(key, out var value))
            {
                await AsyncSemaphore.WaitAsync();
                try
                {
                    var newValue = await populatorFunc();
                    lock (storageLock)
                    {
                        if (!TryGet(key, out value))
                        {
                            value = newValue;
                            Storage[key] = new TCachedItem { Value = value };
                        }
                    }
                }
                finally
                {
                    AsyncSemaphore.Release();
                }
            }

            if (CleansSemaphore <= 0 && NeedsClean())
            {
                CleansSemaphore++;
                _ = Task.Run(() => Clean()).ContinueWith(t => { CleansSemaphore--; });
            }

            return value;
        }

        /// <see cref="ICache.Clear"/>
        public void Clear()
        {
            lock (storageLock)
            {
                Storage.Clear();
            }
        }

        /// <see cref="ICache{TKey}.Remove(TKey[])"/>
        public void Remove(params TKey[] keys)
        {
            Remove(keys.AsEnumerable());
        }

        /// <see cref="ICache{TKey}.Remove(IEnumerable{TKey})"/>
        public void Remove(IEnumerable<TKey> keys)
        {
            foreach (var key in keys)
            {
                lock (storageLock)
                {
                    if (HasValue(key))
                    {
                        Storage.Remove(key);
                    }
                }
            }
        }

        /// <inheritdoc />
        public virtual void Clean()
        {
            CleansSemaphore++;

            // Remove invalid items.
            CleanBy(Storage.Where(kv => !HasValue(kv.Key)).Select(kv => kv.Key));

            CleansSemaphore--;
        }

        /// <summary>
        /// Removes the given key values from storage.
        /// </summary>
        protected void CleanBy(IEnumerable<TKey> selector)
        {
            List<TKey> keys;
            lock (storageLock)
            {
                keys = selector.ToList();
            }

            foreach (var key in keys)
            {
                lock (storageLock)
                {
                    Storage.Remove(key);
                }
            }
        }
    }
}
