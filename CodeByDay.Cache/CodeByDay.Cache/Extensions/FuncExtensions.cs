﻿using System;

namespace CodeByDay.Cache.Extensions
{
    /// <summary>
    /// Extentions for Func&lt;TReturn&gt;
    /// </summary>
    public static class FuncExtensions
    {
        private static ICache<Delegate, Object> noParamCache = new Cache<Delegate, Object>();
        private static ICache<Delegate, ICache<Object, Object>> paramCache = new Cache<Delegate, ICache<Object, Object>>();

        /// <summary>
        /// Removes all keys from the function storage caches.
        /// </summary>
        public static void Clear()
        {
            noParamCache.Clear();
            paramCache.Clear();
        }

        private static TReturn ExecuteCachedInternal<TReturn>(object key, Delegate func, Func<Object> valueProducer)
        {
            var cache = paramCache.Get(func, () => new Cache<Object, Object>());
            return (TReturn)cache.Get(key, valueProducer);
        }

        // 0
        /// <summary>
        /// If the given functions has already been executed, returns the previous result. Otherwise, executes the function and stores the result.
        /// </summary>
        public static TReturn ExecuteCached<TReturn>(this Func<TReturn> func)
        {
            return (TReturn)noParamCache.Get(func, () => func());
        }

        // 1
        /// <summary>
        /// If the given functions has already been executed with the given parameters, returns the previous result. Otherwise, executes the function and stores the result.
        /// </summary>
        public static TReturn ExecuteCached<TArg1, TReturn>(
            this Func<TArg1, TReturn> func,
            TArg1 arg1)
        {
            return ExecuteCachedInternal<TReturn>(arg1, func,
                () => func(arg1));
        }

        // 2
        /// <summary>
        /// If the given functions has already been executed with the given parameters, returns the previous result. Otherwise, executes the function and stores the result.
        /// </summary>
        public static TReturn ExecuteCached<TArg1, TArg2, TReturn>(
            this Func<TArg1, TArg2, TReturn> func,
            TArg1 arg1, TArg2 arg2)
        {
            var tupleKey = Tuple.Create(arg1, arg2);
            return ExecuteCachedInternal<TReturn>(tupleKey, func,
                () => func(arg1, arg2));
        }

        // 3
        /// <summary>
        /// If the given functions has already been executed with the given parameters, returns the previous result. Otherwise, executes the function and stores the result.
        /// </summary>
        public static TReturn ExecuteCached<TArg1, TArg2, TArg3, TReturn>(
            this Func<TArg1, TArg2, TArg3, TReturn> func,
            TArg1 arg1, TArg2 arg2, TArg3 arg3)
        {
            var tupleKey = Tuple.Create(arg1, arg2, arg3);
            return ExecuteCachedInternal<TReturn>(tupleKey, func,
                () => func(arg1, arg2, arg3));
        }

        // 4
        /// <summary>
        /// If the given functions has already been executed with the given parameters, returns the previous result. Otherwise, executes the function and stores the result.
        /// </summary>
        public static TReturn ExecuteCached<TArg1, TArg2, TArg3, TArg4, TReturn>(
            this Func<TArg1, TArg2, TArg3, TArg4, TReturn> func,
            TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4)
        {
            var tupleKey = Tuple.Create(arg1, arg2, arg3, arg4);
            return ExecuteCachedInternal<TReturn>(tupleKey, func,
                () => func(arg1, arg2, arg3, arg4));
        }

        // 5
        /// <summary>
        /// If the given functions has already been executed with the given parameters, returns the previous result. Otherwise, executes the function and stores the result.
        /// </summary>
        public static TReturn ExecuteCached<TArg1, TArg2, TArg3, TArg4, TArg5, TReturn>(
            this Func<TArg1, TArg2, TArg3, TArg4, TArg5, TReturn> func,
            TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5)
        {
            var tupleKey = Tuple.Create(arg1, arg2, arg3, arg4, arg5);
            return ExecuteCachedInternal<TReturn>(tupleKey, func,
                () => func(arg1, arg2, arg3, arg4, arg5));
        }

        // 6
        /// <summary>
        /// If the given functions has already been executed with the given parameters, returns the previous result. Otherwise, executes the function and stores the result.
        /// </summary>
        public static TReturn ExecuteCached<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TReturn>(
            this Func<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TReturn> func,
            TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5, TArg6 arg6)
        {
            var tupleKey = Tuple.Create(arg1, arg2, arg3, arg4, arg5, arg6);
            return ExecuteCachedInternal<TReturn>(tupleKey, func,
                () => func(arg1, arg2, arg3, arg4, arg5, arg6));
        }

        // 7
        /// <summary>
        /// If the given functions has already been executed with the given parameters, returns the previous result. Otherwise, executes the function and stores the result.
        /// </summary>
        public static TReturn ExecuteCached<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TReturn>(
            this Func<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TReturn> func,
            TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5, TArg6 arg6, TArg7 arg7)
        {
            var tupleKey = Tuple.Create(arg1, arg2, arg3, arg4, arg5, arg6, arg7);
            return ExecuteCachedInternal<TReturn>(tupleKey, func,
                () => func(arg1, arg2, arg3, arg4, arg5, arg6, arg7));
        }

        // 8
        /// <summary>
        /// If the given functions has already been executed with the given parameters, returns the previous result. Otherwise, executes the function and stores the result.
        /// </summary>
        public static TReturn ExecuteCached<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TReturn>(
            this Func<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TReturn> func,
            TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5, TArg6 arg6, TArg7 arg7, TArg8 arg8)
        {
            var tupleKey = Tuple.Create(arg1, arg2, arg3, arg4, arg5, arg6, arg7, Tuple.Create(arg8));
            return ExecuteCachedInternal<TReturn>(tupleKey, func,
                () => func(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8));
        }

        // 9
        /// <summary>
        /// If the given functions has already been executed with the given parameters, returns the previous result. Otherwise, executes the function and stores the result.
        /// </summary>
        public static TReturn ExecuteCached<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TReturn>(
            this Func<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TReturn> func,
            TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5, TArg6 arg6, TArg7 arg7, TArg8 arg8, TArg9 arg9)
        {
            var tupleKey = Tuple.Create(arg1, arg2, arg3, arg4, arg5, arg6, arg7, Tuple.Create(arg8, arg9));
            return ExecuteCachedInternal<TReturn>(tupleKey, func,
                () => func(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9));
        }

        // 10
        /// <summary>
        /// If the given functions has already been executed with the given parameters, returns the previous result. Otherwise, executes the function and stores the result.
        /// </summary>
        public static TReturn ExecuteCached<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10, TReturn>(
            this Func<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10, TReturn> func,
            TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5, TArg6 arg6, TArg7 arg7, TArg8 arg8, TArg9 arg9, TArg10 arg10)
        {
            var tupleKey = Tuple.Create(arg1, arg2, arg3, arg4, arg5, arg6, arg7, Tuple.Create(arg8, arg9, arg10));
            return ExecuteCachedInternal<TReturn>(tupleKey, func,
                () => func(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10));
        }

        // 11
        /// <summary>
        /// If the given functions has already been executed with the given parameters, returns the previous result. Otherwise, executes the function and stores the result.
        /// </summary>
        public static TReturn ExecuteCached<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10, TArg11, TReturn>(
            this Func<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10, TArg11, TReturn> func,
            TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5, TArg6 arg6, TArg7 arg7, TArg8 arg8, TArg9 arg9, TArg10 arg10, TArg11 arg11)
        {
            var tupleKey = Tuple.Create(arg1, arg2, arg3, arg4, arg5, arg6, arg7, Tuple.Create(arg8, arg9, arg10, arg11));
            return ExecuteCachedInternal<TReturn>(tupleKey, func,
                () => func(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11));
        }

        // 12
        /// <summary>
        /// If the given functions has already been executed with the given parameters, returns the previous result. Otherwise, executes the function and stores the result.
        /// </summary>
        public static TReturn ExecuteCached<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10, TArg11, TArg12, TReturn>(
            this Func<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10, TArg11, TArg12, TReturn> func,
            TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5, TArg6 arg6, TArg7 arg7, TArg8 arg8, TArg9 arg9, TArg10 arg10, TArg11 arg11, TArg12 arg12)
        {
            var tupleKey = Tuple.Create(arg1, arg2, arg3, arg4, arg5, arg6, arg7, Tuple.Create(arg8, arg9, arg10, arg11, arg12));
            return ExecuteCachedInternal<TReturn>(tupleKey, func,
                () => func(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12));
        }

        // 13
        /// <summary>
        /// If the given functions has already been executed with the given parameters, returns the previous result. Otherwise, executes the function and stores the result.
        /// </summary>
        public static TReturn ExecuteCached<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10, TArg11, TArg12, TArg13, TReturn>(
            this Func<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10, TArg11, TArg12, TArg13, TReturn> func,
            TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5, TArg6 arg6, TArg7 arg7, TArg8 arg8, TArg9 arg9, TArg10 arg10, TArg11 arg11, TArg12 arg12, TArg13 arg13)
        {
            var tupleKey = Tuple.Create(arg1, arg2, arg3, arg4, arg5, arg6, arg7, Tuple.Create(arg8, arg9, arg10, arg11, arg12, arg13));
            return ExecuteCachedInternal<TReturn>(tupleKey, func,
                () => func(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13));
        }

        // 14
        /// <summary>
        /// If the given functions has already been executed with the given parameters, returns the previous result. Otherwise, executes the function and stores the result.
        /// </summary>
        public static TReturn ExecuteCached<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10, TArg11, TArg12, TArg13, TArg14, TReturn>(
            this Func<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10, TArg11, TArg12, TArg13, TArg14, TReturn> func,
            TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5, TArg6 arg6, TArg7 arg7, TArg8 arg8, TArg9 arg9, TArg10 arg10, TArg11 arg11, TArg12 arg12, TArg13 arg13, TArg14 arg14)
        {
            var tupleKey = Tuple.Create(arg1, arg2, arg3, arg4, arg5, arg6, arg7, Tuple.Create(arg8, arg9, arg10, arg11, arg12, arg13, arg14));
            return ExecuteCachedInternal<TReturn>(tupleKey, func,
                () => func(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13, arg14));
        }

        // 15
        /// <summary>
        /// If the given functions has already been executed with the given parameters, returns the previous result. Otherwise, executes the function and stores the result.
        /// </summary>
        public static TReturn ExecuteCached<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10, TArg11, TArg12, TArg13, TArg14, TArg15, TReturn>(
            this Func<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10, TArg11, TArg12, TArg13, TArg14, TArg15, TReturn> func,
            TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5, TArg6 arg6, TArg7 arg7, TArg8 arg8, TArg9 arg9, TArg10 arg10, TArg11 arg11, TArg12 arg12, TArg13 arg13, TArg14 arg14, TArg15 arg15)
        {
            var tupleKey = Tuple.Create(arg1, arg2, arg3, arg4, arg5, arg6, arg7, Tuple.Create(arg8, arg9, arg10, arg11, arg12, arg13, arg14, Tuple.Create(arg15)));
            return ExecuteCachedInternal<TReturn>(tupleKey, func,
                () => func(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13, arg14, arg15));
        }

        // 16
        /// <summary>
        /// If the given functions has already been executed with the given parameters, returns the previous result. Otherwise, executes the function and stores the result.
        /// </summary>
        public static TReturn ExecuteCached<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10, TArg11, TArg12, TArg13, TArg14, TArg15, TArg16, TReturn>(
            this Func<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10, TArg11, TArg12, TArg13, TArg14, TArg15, TArg16, TReturn> func,
            TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5, TArg6 arg6, TArg7 arg7, TArg8 arg8, TArg9 arg9, TArg10 arg10, TArg11 arg11, TArg12 arg12, TArg13 arg13, TArg14 arg14, TArg15 arg15, TArg16 arg16)
        {
            var tupleKey = Tuple.Create(arg1, arg2, arg3, arg4, arg5, arg6, arg7, Tuple.Create(arg8, arg9, arg10, arg11, arg12, arg13, arg14, Tuple.Create(arg15, arg16)));
            return ExecuteCachedInternal<TReturn>(tupleKey, func,
                () => func(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13, arg14, arg15, arg16));
        }
    }
}