﻿namespace CodeByDay.Cache
{
    /// <summary>
    /// Allows the removal of invalid items from storage.
    /// </summary>
    public interface ICleanable
    {
        /// <summary>
        /// Removes invalid items from storage.
        /// </summary>
        void Clean();
    }
}
