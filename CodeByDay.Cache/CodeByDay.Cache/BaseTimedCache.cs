﻿using CodeByDay.Cache.CachedItem;
using CodeByDay.Cache.Storage;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CodeByDay.Cache
{
    /// <summary>
    /// Cache that stores values which expire after an amount of time. TValues are stored in a dictionary and are retrieved by the TKey.
    /// </summary>
    /// <typeparam name="TKey">The key type. Should likely be String.</typeparam>
    /// <typeparam name="TValue">The value type.</typeparam>
    /// <typeparam name="TCachedItem">The kind of ICachedItem to store.</typeparam>
    public abstract class BaseTimedCache<TKey, TValue, TCachedItem> : BaseCache<TKey, TValue, TCachedItem>, ITimedCache<TKey, TValue> where TCachedItem : ICachedItem<TValue>, ITimedCachedItem, new()
    {
        /// <summary>
        /// Key-values kept for more than this amout of time will be considered not valid. Defaults to 1 hour.
        /// </summary>
        public TimeSpan TimeToKeep { get; set; } = TimeSpan.FromHours(1);

        private DateTime lastClean = DateTime.Now;

        /// <summary>
        /// Creates a new BaseTimedCache.
        /// </summary>
        /// <param name="storage">The item which maintains the internal storage.</param>
        protected BaseTimedCache(IStorage<TKey, TCachedItem> storage) : base(storage) { }

        /// <summary>
        /// Returns true if this cache needs to be cleaned.
        /// </summary>
        protected override Boolean NeedsClean() => base.NeedsClean() || DateTime.Now - lastClean >= TimeToKeep;

        /// <see cref="BaseCache{TKey, TValue, TCachedItem}.HasValue"/>
        protected override bool HasValue(TKey key)
        {
            lock (storageLock)
            {
                if (!base.HasValue(key))
                {
                    return false;
                }

                if (!Storage[key].TimeToKeep.HasValue && (DateTime.Now - Storage[key].CreatedTime) >= TimeToKeep)
                {
                    Storage[key].IsValid = false;
                }

                return Storage[key].IsValid;
            }
        }

        /// <see cref="ITimedCache{TKey, TValue}.Get(TKey, TimeSpan, Func{TValue})"/>
        public TValue Get(TKey key, TimeSpan timeToKeep, Func<TValue> populatorFunc)
        {
            return Get(key, () => new KeyValuePair<TimeSpan, TValue>(timeToKeep, populatorFunc()));
        }

        /// <see cref="ITimedCache{TKey, TValue}.Get(TKey, Func{KeyValuePair{TimeSpan, TValue}})"/>
        public TValue Get(TKey key, Func<KeyValuePair<TimeSpan, TValue>> populatorFunc)
        {
            TValue value;
            lock (storageLock)
            {
                if (!TryGet(key, out value))
                {
                    var valueTuple = populatorFunc();
                    value = valueTuple.Value;
                    Storage[key] = new TCachedItem
                    {
                        Value = value,
                        TimeToKeep = valueTuple.Key
                    };

                    // Ensure that this cached item gets cleaned once it expires.
                    if (valueTuple.Key < TimeToKeep)
                    {
                        var psudoLastClean = DateTime.Now - TimeToKeep + valueTuple.Key;
                        lastClean = psudoLastClean < lastClean ? psudoLastClean : lastClean;
                    }
                }
            }

            if (CleansSemaphore <= 0 && NeedsClean())
            {
                CleansSemaphore++;
                Task.Run(() => Clean()).ContinueWith(t => { CleansSemaphore--; });
            }

            return value;
        }

        /// <see cref="ITimedCache{TKey, TValue}.Get(TKey, Func{ValueTuple{TimeSpan, TValue}})"/>
        public TValue Get(TKey key, Func<(TimeSpan, TValue)> populatorFunc)
        {
            return Get(key, () =>
            {
                var (ts, val) = populatorFunc();
                return new KeyValuePair<TimeSpan, TValue>(ts, val);
            });
        }

        private System.Threading.SemaphoreSlim AsyncSemaphore { get; } = new System.Threading.SemaphoreSlim(1, 1);

        /// <see cref="ITimedCache{TKey, TValue}.GetAsync(TKey, TimeSpan, Func{Task{TValue}})"/>
        public Task<TValue> GetAsync(TKey key, TimeSpan timeToKeep, Func<Task<TValue>> populatorFunc)
        {
            return GetAsync(key, async () => new KeyValuePair<TimeSpan, TValue>(timeToKeep, await populatorFunc()));
        }

        /// <see cref="ITimedCache{TKey, TValue}.GetAsync(TKey, Func{Task{KeyValuePair{TimeSpan, TValue}}})"/>
        public async Task<TValue> GetAsync(TKey key, Func<Task<KeyValuePair<TimeSpan, TValue>>> populatorFunc)
        {
            if (!TryGet(key, out var value))
            {
                await AsyncSemaphore.WaitAsync();
                try
                {
                    var valueTuple = await populatorFunc();
                    lock (storageLock)
                    {
                        if (!TryGet(key, out value))
                        {
                            value = valueTuple.Value;
                            Storage[key] = new TCachedItem
                            {
                                Value = value,
                                TimeToKeep = valueTuple.Key
                            };

                            // Ensure that this cached item gets cleaned once it expires.
                            if (valueTuple.Key < TimeToKeep)
                            {
                                var psudoLastClean = DateTime.Now - TimeToKeep + valueTuple.Key;
                                lastClean = psudoLastClean < lastClean ? psudoLastClean : lastClean;
                            }
                        }
                    }
                }
                finally
                {
                    AsyncSemaphore.Release();
                }
            }

            if (CleansSemaphore <= 0 && NeedsClean())
            {
                CleansSemaphore++;
                _ = Task.Run(() => Clean()).ContinueWith(t => { CleansSemaphore--; });
            }

            return value;
        }

        /// <see cref="ITimedCache{TKey, TValue}.GetAsync(TKey, Func{Task{ValueTuple{TimeSpan, TValue}}})"/>
        public Task<TValue> GetAsync(TKey key, Func<Task<(TimeSpan, TValue)>> populatorFunc)
        {
            return GetAsync(key, async () =>
            {
                var (ts, val) = await populatorFunc();
                return new KeyValuePair<TimeSpan, TValue>(ts, val);
            });
        }

        /// <inheritdoc />
        public override void Clean()
        {
            CleansSemaphore++;

            lastClean = DateTime.Now;
            base.Clean();

            CleansSemaphore--;
        }
    }
}
