﻿#if !RELEASE
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("CodeByDay.Cache.Test")]

namespace CodeByDay.Cache
{
    internal interface ITestInterfaces
    {
        bool StorageIsWeak();

        bool CleanIsRunning();
    }

    internal interface IWeak { }
}
#endif