﻿using System;

namespace CodeByDay.Cache.CachedItem
{
    /// <summary>
    /// A BaseCachedItem which remembers its creation time.
    /// </summary>
    /// <typeparam name="TValue">The type of the value.</typeparam>
    public class CreatedCachedItem<TValue> : BaseCachedItem<TValue>, ICreatedCachedItem
    {
        /// <see cref="ICreatedCachedItem.CreatedTime"/>
        public DateTime CreatedTime { get; set; }

        /// <summary>
        /// Creates a new CreatedCachedItem.
        /// </summary>
        public CreatedCachedItem()
        {
            CreatedTime = DateTime.Now;
        }
    }
}
