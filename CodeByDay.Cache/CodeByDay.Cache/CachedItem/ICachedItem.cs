﻿namespace CodeByDay.Cache.CachedItem
{
    /// <summary>
    /// An Object which can be invalidated.
    /// </summary>
    public interface ICachedItem
    {
        /// <summary>
        /// If this item is considered usable.
        /// </summary>
        bool IsValid { get; set; }
    }

    /// <summary>
    /// An Object which holds a value.
    /// </summary>
    /// <typeparam name="TValue">The type of the value.</typeparam>
    public interface ICachedItem<TValue> : ICachedItem
    {
        /// <summary>
        /// The value created with this Object.
        /// </summary>
        TValue Value { get; set; }
    }
}
