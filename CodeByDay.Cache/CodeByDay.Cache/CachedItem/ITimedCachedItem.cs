﻿using System;

namespace CodeByDay.Cache.CachedItem
{
    /// <summary>
    /// An Object which should only be kept for a TimeSpan.
    /// </summary>
    public interface ITimedCachedItem : ICreatedCachedItem
    {
        /// <summary>
        /// The length of time to keep this item.
        /// </summary>
        TimeSpan? TimeToKeep { get; set; }
    }
}
