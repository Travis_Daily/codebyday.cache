﻿using System;

namespace CodeByDay.Cache.CachedItem
{
    /// <summary>
    /// A UsefulCachedItem which keeps track of when it should be removed.
    /// </summary>
    /// <typeparam name="TValue">The type of the value.</typeparam>
    public class FullyCachedItem<TValue> : UsefulCachedItem<TValue>, ITimedCachedItem
    {
        /// <see cref="ITimedCachedItem.TimeToKeep"/>
        public TimeSpan? TimeToKeep { get; set; }

        /// <see cref="ICachedItem.IsValid"/>
        public override bool IsValid
        {
            get
            {
                if (base.IsValid && TimeToKeep.HasValue && (DateTime.Now - CreatedTime) >= TimeToKeep.Value)
                {
                    base.IsValid = false;
                }

                return base.IsValid;
            }
            set
            {
                base.IsValid = value;
            }
        }
    }
}
