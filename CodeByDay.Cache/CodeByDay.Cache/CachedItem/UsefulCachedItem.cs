﻿using System;

namespace CodeByDay.Cache.CachedItem
{
    /// <summary>
    /// A CreatedCachedItem which counts the number of times the Value has been accessed.
    /// </summary>
    /// <typeparam name="TValue">The type of the value.</typeparam>
    public class UsefulCachedItem<TValue> : CreatedCachedItem<TValue>, IUsefulCachedItem
    {
        /// <see cref="ICachedItem{TValue}.Value"/>
        public override TValue Value
        {
            get
            {
                AccessCount++;
                LastUsefulness = null;
                return base.Value;
            }
            set
            {
                AccessCount = 0;
                LastUsefulness = null;
                base.Value = value;
            }
        }

        /// <summary>
        /// Prevents the serialization of the overwritten Value property. Instead, BaseValue is used. This prevents serialization from causing side-effects that the overwritten property causes.
        /// </summary>
        /// <returns>false</returns>
        public bool ShouldSerializeValue()
        {
            return false;
        }

        /// <summary>
        /// A direct link to the base object's Value property. This is intended to be used during serialization.
        /// </summary>
        [Obsolete("Use Value.", true)]
        public TValue BaseValue
        {
            get => base.Value;
            set => base.Value = value;
        }

        /// <see cref="IUsefulCachedItem.AccessCount"/>
        public int AccessCount { get; set; }

        /// <see cref="IUsefulCachedItem.LastUsefulness"/>
        public double? LastUsefulness { get; set; }
    }
}
