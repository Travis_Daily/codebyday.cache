﻿using System.ComponentModel;

namespace CodeByDay.Cache.CachedItem
{
    /// <summary>
    /// An Object which has a value and can be invalidated.
    /// </summary>
    /// <typeparam name="TValue">The type of the value.</typeparam>
    public class BaseCachedItem<TValue> : ICachedItem<TValue>
    {
        /// <see cref="ICachedItem{TValue}.Value"/>
        public virtual TValue Value { get; set; }

        /// <see cref="ICachedItem.IsValid"/>
        [DefaultValue(true)]
        public virtual bool IsValid { get; set; } = true;
    }
}
