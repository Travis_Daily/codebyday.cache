﻿namespace CodeByDay.Cache.CachedItem
{
    /// <summary>
    /// An Object which counts the number of times its value has been accessed.
    /// </summary>
    public interface IUsefulCachedItem : ICreatedCachedItem
    {
        /// <summary>
        /// The number of times the Value of this item has been accessed.
        /// </summary>
        int AccessCount { get; }

        /// <summary>
        /// The last recorded usefulness of this item. Should be nullified when this may have changed.
        /// </summary>
        double? LastUsefulness { get; set; }
    }
}
