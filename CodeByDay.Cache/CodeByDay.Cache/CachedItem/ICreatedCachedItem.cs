﻿using System;

namespace CodeByDay.Cache.CachedItem
{
    /// <summary>
    /// An Object which has a created time.
    /// </summary>
    public interface ICreatedCachedItem : ICachedItem
    {
        /// <summary>
        /// The DateTime at which this Object was created.
        /// </summary>
        DateTime CreatedTime { get; }
    }
}
