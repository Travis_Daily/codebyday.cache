﻿using CodeByDay.Cache.CachedItem;
using CodeByDay.Cache.Storage;
using System;
using System.Collections.Generic;

namespace CodeByDay.Cache
{
    /// <summary>
    /// A cache with weak references to its cached TValues. TValues are stored in a dictionary and are retrieved by the TKey.
    /// </summary>
    /// <typeparam name="TKey">The key type. Should likely be String.</typeparam>
    /// <typeparam name="TValue">The value type.</typeparam>
    [Obsolete("Use 'new Cache<TKey, TValue>(new WeakDictionaryStorage<TKey, BaseCachedItem<TValue>>())'.)", false)]
    public sealed class WeakCache<TKey, TValue> : BaseCache<TKey, TValue, BaseCachedItem<TValue>>
    {
        /// <summary>
        /// Creates a new Cache of WeakReference objects.
        /// </summary>
        public WeakCache() : this(null) { }

        /// <summary>
        /// Creates a new Cache of WeakReference objects.
        /// </summary>
        /// <param name="comparer">A IEqualityComparer to find keys with.</param>
        public WeakCache(IEqualityComparer<TKey> comparer) : base(new WeakDictionaryStorage<TKey, BaseCachedItem<TValue>>(comparer)) { }
    }
}
