﻿using CodeByDay.Cache.CachedItem;
using CodeByDay.Cache.Storage;
using CodeByDay.Cache.Usefulness;
using System;

namespace CodeByDay.Cache
{
    /// <summary>
    /// Cache that only stores a given number of items which expire after an amount of time. TValues are stored in a dictionary and are retrieved by the TKey.
    /// </summary>
    /// <typeparam name="TKey">The key type. Should likely be String.</typeparam>
    /// <typeparam name="TValue">The value type.</typeparam>
    public class TimedItemedCache<TKey, TValue> : BaseTimedCache<TKey, TValue, FullyCachedItem<TValue>>, IItemedCache<TKey, TValue>
    {
        /// <summary>
        /// At most, this many key-values will be kept after a Clean(). Defaults to 100.
        /// </summary>
        public int ItemsToKeep { get; set; } = 100;

        /// <see cref="IItemedCache.UsefulnessProvider"/>
        public IUsefulnessProvider UsefulnessProvider { get; set; } = new LinearUsefulnessProvider();

        /// <summary>
        /// Creates a new TimedItemedCache.
        /// </summary>
        public TimedItemedCache() : this(new DictionaryStorage<TKey, FullyCachedItem<TValue>>()) { }

        /// <summary>
        /// Creates a new TimedItemedCache.
        /// </summary>
        /// <param name="storage">The item which maintains the internal storage.</param>
        public TimedItemedCache(IStorage<TKey, FullyCachedItem<TValue>> storage) : base(storage) { }

        /// <summary>
        /// Returns true if this cache needs to be cleaned.
        /// </summary>
        protected override Boolean NeedsClean() => base.NeedsClean() || Storage.Count > ItemsToKeep;

        /// <inheritdoc />
        public override void Clean()
        {
            CleansSemaphore++;

            base.Clean();
            CleanBy(UsefulnessProvider.FindUseless(Storage, ItemsToKeep));

            CleansSemaphore--;
        }
    }
}
