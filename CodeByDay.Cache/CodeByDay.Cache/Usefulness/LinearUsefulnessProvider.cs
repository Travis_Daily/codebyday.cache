﻿using CodeByDay.Cache.CachedItem;
using System;

namespace CodeByDay.Cache.Usefulness
{
    /// <summary>
    /// An Object which evaluates the usefulness of a cached item.
    /// </summary>
    public class LinearUsefulnessProvider : UsefulnessProvider
    {
        /// <summary>
        /// Gets the usefulness of the given item by dividing the number of times the item has been access over the number of milliseconds it has spent alive 
        /// </summary>
        /// <param name="val">The ICachedItem to get the usefulness of.</param>
        /// <param name="referenceTime">A DateTime use evaluate against.</param>
        public override double GetUsefulness(IUsefulCachedItem val, DateTime referenceTime)
        {
            var msAlive = Math.Max((referenceTime - val.CreatedTime).TotalMilliseconds, 0.0);
            return Math.Max(val.AccessCount, 1) / msAlive;
        }
    }
}
