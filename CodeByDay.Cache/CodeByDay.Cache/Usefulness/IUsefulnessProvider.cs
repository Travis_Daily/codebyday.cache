﻿using CodeByDay.Cache.CachedItem;
using CodeByDay.Cache.Storage;
using System;
using System.Collections.Generic;

namespace CodeByDay.Cache.Usefulness
{
    /// <summary>
    /// An Object which evaluates the usefulness of a cached item.
    /// </summary>
    public interface IUsefulnessProvider
    {
        /// <summary>
        /// Gets a double which equates to the relative usefulness of the given item. Higher values are more useful.
        /// </summary>
        /// <param name="val">The ICachedItem to get the usefulness of.</param>
        /// <param name="referenceTime">A DateTime use evaluate against.</param>
        double GetUsefulness(IUsefulCachedItem val, DateTime referenceTime);

        /// <summary>
        /// Evaluates the given IStorage to find its useless keys.
        /// </summary>
        /// <typeparam name="TKey">The key type of the IStorage.</typeparam>
        /// <typeparam name="TCachedItem">The Cached value type of the IStorage.</typeparam>
        /// <param name="Storage">The IStorage to select the keys from.</param>
        /// <param name="ItemsToKeep">The number of items which should stay in the IStorage.</param>
        /// <returns>An enumeration of the keys to remove.</returns>
        IEnumerable<TKey> FindUseless<TKey, TCachedItem>(IStorage<TKey, TCachedItem> Storage, int ItemsToKeep) where TCachedItem : IUsefulCachedItem;
    }
}
