﻿using CodeByDay.Cache.CachedItem;
using CodeByDay.Cache.Storage;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CodeByDay.Cache.Usefulness
{
    /// <summary>
    /// An Object which evaluates the usefulness of a cached item.
    /// </summary>
    public abstract class UsefulnessProvider : IUsefulnessProvider
    {
        /// <inheritdoc />
        public abstract double GetUsefulness(IUsefulCachedItem val, DateTime referenceTime);

        /// <inheritdoc />
        public IEnumerable<TKey> FindUseless<TKey, TCachedItem>(IStorage<TKey, TCachedItem> Storage, int ItemsToKeep) where TCachedItem : IUsefulCachedItem
        {
            if (Storage.Count <= ItemsToKeep)
            {
                yield break;
            }

            var yieldedKeys = new List<TKey>();

            var yieldCount = 0;
            foreach (var k in Storage.Where(x => x.Value.LastUsefulness != null).OrderByDescending(x => x.Value.LastUsefulness).Skip(ItemsToKeep).Select(kv => kv.Key))
            {
                yieldCount++;
                yieldedKeys.Add(k);
                yield return k;
            }

            if (yieldCount < Storage.Count - ItemsToKeep)
            {
                var startTime = DateTime.Now;
                foreach (var kv in Storage.Where(kv => !yieldedKeys.Contains(kv.Key)))
                {
                    kv.Value.LastUsefulness = GetUsefulness(kv.Value, startTime);
                }

                foreach (var k in Storage.Where(kv => !yieldedKeys.Contains(kv.Key)).OrderByDescending(x => x.Value.LastUsefulness).Skip(ItemsToKeep).Select(kv => kv.Key))
                {
                    yield return k;
                }
            }
        }
    }
}
