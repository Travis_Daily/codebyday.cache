﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CodeByDay.Cache
{
    /// <summary>
    /// Cache to store values which expire after an amount of time.
    /// </summary>
    public interface ITimedCache : ICache, ICleanable
    {
        /// <summary>
        /// Key-values kept for more than this amout of time will be considered not valid.
        /// </summary>
        TimeSpan TimeToKeep { get; set; }
    }

    /// <summary>
    /// Cache to store values which expire after an amount of time.
    /// </summary>
    public interface ITimedCache<TKey, TValue> : ITimedCache, ICache<TKey, TValue>
    {
        /// <summary>
        /// Gets the value associated with the given TKey.
        /// If it doesn't exist or is not valid, the Func is executed to populate the value.
        /// </summary>
        /// <param name="key">The key value to look for.</param>
        /// <param name="timeToKeep">The time to keep the value for.</param>
        /// <param name="populatorFunc">A function to create a TValue if the TKey isn't found or is not valid.</param>
        /// <returns>The cached TValue or the TValue created from the Func if the TKey did not exist or was not valid.</returns>
        TValue Get(TKey key, TimeSpan timeToKeep, Func<TValue> populatorFunc);

        /// <summary>
        /// Gets the value associated with the given TKey.
        /// If it doesn't exist or is not valid, the Func is executed to populate the value and sets its time to keep.
        /// </summary>
        /// <param name="key">The key value to look for.</param>
        /// <param name="populatorFunc">A function to create a TValue if the TKey isn't found or is not valid.</param>
        /// <returns>The cached TValue or the TValue created from the Func if the TKey did not exist or was not valid.</returns>
        TValue Get(TKey key, Func<KeyValuePair<TimeSpan, TValue>> populatorFunc);

        /// <summary>
        /// Gets the value associated with the given TKey.
        /// If it doesn't exist or is not valid, the Func is executed to populate the value and sets its time to keep.
        /// </summary>
        /// <param name="key">The key value to look for.</param>
        /// <param name="populatorFunc">A function to create a TValue if the TKey isn't found or is not valid.</param>
        /// <returns>The cached TValue or the TValue created from the Func if the TKey did not exist or was not valid.</returns>
        TValue Get(TKey key, Func<(TimeSpan, TValue)> populatorFunc);

        /// <summary>
        /// Gets the value associated with the given TKey.
        /// If it doesn't exist or is not valid, the Func is executed to populate the value.
        /// </summary>
        /// <param name="key">The key value to look for.</param>
        /// <param name="timeToKeep">The time to keep the value for.</param>
        /// <param name="populatorFunc">A function to create a TValue if the TKey isn't found or is not valid.</param>
        /// <returns>The cached TValue or the TValue created from the Func if the TKey did not exist or was not valid.</returns>
        Task<TValue> GetAsync(TKey key, TimeSpan timeToKeep, Func<Task<TValue>> populatorFunc);

        /// <summary>
        /// Gets the value associated with the given TKey.
        /// If it doesn't exist or is not valid, the Func is executed to populate the value and sets its time to keep.
        /// </summary>
        /// <param name="key">The key value to look for.</param>
        /// <param name="populatorFunc">A function to create a TValue if the TKey isn't found or is not valid.</param>
        /// <returns>The cached TValue or the TValue created from the Func if the TKey did not exist or was not valid.</returns>
        Task<TValue> GetAsync(TKey key, Func<Task<KeyValuePair<TimeSpan, TValue>>> populatorFunc);

        /// <summary>
        /// Gets the value associated with the given TKey.
        /// If it doesn't exist or is not valid, the Func is executed to populate the value and sets its time to keep.
        /// </summary>
        /// <param name="key">The key value to look for.</param>
        /// <param name="populatorFunc">A function to create a TValue if the TKey isn't found or is not valid.</param>
        /// <returns>The cached TValue or the TValue created from the Func if the TKey did not exist or was not valid.</returns>
        Task<TValue> GetAsync(TKey key, Func<Task<(TimeSpan, TValue)>> populatorFunc);
    }
}
